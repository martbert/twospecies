#!/usr/bin/env python

import sys, getopt, os
import numpy as np
from scipy import optimize
import re

import matplotlib
matplotlib.use('agg')
import pylab as pl
#import matplotlib_params
import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['lines.markersize'] = 6
mpl.rcParams['lines.markeredgewidth'] = 1.0
mpl.rcParams['figure.figsize'] = (7,4)
mpl.rcParams['figure.subplot.bottom'] = 0.15
mpl.rcParams['font.size'] = 14.0
#mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 1.0
mpl.rcParams['legend.fontsize'] = 14.0
mpl.rcParams['figure.subplot.right'] = 0.7
mpl.rcParams['figure.subplot.left'] = 0.12
mpl.rcParams['figure.subplot.top'] = 0.95
mpl.rcParams['axes.color_cycle'] = ['#000000','#E69F00','#56B4E9','#009E73','#F0E442','#0072B2','#D55E00','#CC79A7']


###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val
		fileListName, fileExtension = os.path.splitext(fileList)

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b
ftanh = lambda x,a,b,c: a*(np.tanh(b*(x+c))+1.)
dftanh = lambda x,a,b,c: a*b/np.cosh(b*(x+c))**2.
d2ftanh = lambda x,a,b,c: -2.*a*b**2./np.cosh(b*(-c + x))**2.*np.tanh(b*(-c + x))

############################################################
# Main body
############################################################

# Flags
dvskt_flag = 0
savedat_flag = 1

# Percent to skip (brute force initially)
skipfrac = 0.2
dt = 0.005
nmax = 200000

# Load list of filenames
fnames = np.loadtxt(fileList,dtype=str)
try:
	nfiles = len(fnames)
except:
	fnames = [str(fnames)]
	nfiles = len(fnames)

# Prepare to plot all
figA = pl.figure(1)
axA = figA.add_subplot(111)
axA = figA.add_subplot(111)
figB = pl.figure(3)
axB = figB.add_subplot(111)
axB = figB.add_subplot(111)

# Loop over the files, load the data and find the diffusion coefficient
Dbig = np.zeros(nfiles)
Dsmall = np.zeros(nfiles)
kT = np.zeros(nfiles)
ps = []
resA = []
resB = []
header = "#time\t"
for i in range(nfiles):
	fname = fnames[i]
	# Get temperature
	name = fname[fname.rfind('/')+1:]
	print name
	sysvars = np.array(re.findall(r"[-+]?\d*\.\d+|\d+", name)).astype(float)
	big = int(sysvars[-4])
	small = int(sysvars[-3])
	kT[i] = sysvars[-1]
	header += "%.2f\t" % (kT[i])
	# Load data
	data = np.loadtxt(fname)
	npts = data.shape[0]
	print data.shape
	data[:,0] = data[:,0]-data[0,0]
	data[:,0] *= dt
	print data.shape
	print "Timestep = %f" % (data[1,0]-data[0,0])
	big_flag = int(big > 0)
	small_flag = int(small > 0)
	print "Big bool = %d, Small bool = %d" % (big_flag,small_flag)
	t = data[:nmax,0]
	if big_flag:
		axA.plot(data[:,0],data[:,1],label=r'$k_BT=$%.2f' % (kT[i]))
		resA.append(data[:nmax,1])
	if small_flag:
		axB.plot(data[:,0],data[:,2],label=r'$k_BT=$%.2f' % (kT[i]))
		resB.append(data[:nmax,2])
	# Calculate amount of points to skip
	#skip = int(skipfrac*npts)
	skip = 40000
	# Prepare data to fit
	time = data[skip:,0]
	msdbig = data[skip:,1]
	msdsmall = data[skip:,2]
	# Fit the mean square displacement
	parbig,covbig = optimize.curve_fit(flin,time,msdbig,p0=[1.,1.])
	parsmall,covsmall = optimize.curve_fit(flin,time,msdsmall,p0=[1.,1.])
	Dbig[i] = parbig[0]/6.
	Dsmall[i] = parsmall[0]/6.

	#Dbig[i] = msdbig[2*skip]
	#Dsmall[i] = msdsmall[2*skip]

	print "kT = %.2f, Dbig = %f, Dsmall = %f" % (kT[i],Dbig[i],Dsmall[i])

name = fileListName[fileListName.rfind('/')+1:]
print header
if big_flag:
	resA = np.array([t]+resA).T
	print resA.shape
	np.savetxt('results/'+name+'.AMSD.txt',resA,header=header)
if small_flag:
	resB = np.array([t]+resB).T
	np.savetxt('results/'+name+'.BMSD.txt',resB,header=header)

# Plot the results
fig2 = pl.figure(2)
ax2 = fig2.add_subplot(111)
if savedat_flag:
	data = [kT]
# Fits
if big_flag:
	if dvskt_flag:
		parBig,cov = optimize.curve_fit(ftanh,kT,Dbig,p0=[0.25,10.,-0.5])
		x = np.linspace(np.min(kT),np.max(kT),100)
		d2 = d2ftanh(x,parBig[0],parBig[1],parBig[2])
		ixmin = np.argmin(d2)
		xmin = x[ixmin]
		print xmin
		ax2.plot(kT,Dbig/Dbig[-1],'--o',label=r'Big: $T_c=$%f' % (-parBig[2]))
		ax2.plot(x,ftanh(x,parBig[0],parBig[1],parBig[2])/Dbig[-1],'-r')
	else:
		ax2.plot(kT,Dbig,'--o',label=r'Big')
	if savedat_flag:
		data.append(Dbig)
if small_flag:
	if dvskt_flag:
		parSmall,cov = optimize.curve_fit(ftanh,kT,Dsmall,p0=[0.25,10.,-0.5])
		x = np.linspace(np.min(kT),np.max(kT),100)
		d2 = d2ftanh(x,parSmall[-1],parSmall[1],parSmall[2])
		ixmin = np.argmin(d2)
		xmin = x[ixmin]
		print xmin
		ax2.plot(kT,Dsmall/Dsmall[-1],'--s',label=r'Small: $T_c=$%f' % (-parSmall[2]))
		ax2.plot(x,ftanh(x,parSmall[0],parSmall[1],parSmall[2])/Dsmall[-1],'-r')
	else:
		ax2.plot(kT,Dsmall,'--s',label=r'Small')
	if savedat_flag:
		data.append(Dsmall)
ax2.set_xlabel(r'$k_BT$')
ax2.set_ylabel(r'$D$')
ax2.set_yscale('log')
leg = ax2.legend(loc='best',fancybox=True)
leg.get_frame().set_alpha(0.65)

if big_flag:
	axA.set_xlabel(r'$t$')
	axA.set_ylabel(r'$MSD$')
	axA.set_xscale('log')
	axA.set_yscale('log')
	axA.set_xlim(np.min(t),np.max(t))
	#print "Number of lines in plot 1 = %d" % (len(ax1.get_lines()))
	legA = axA.legend(fancybox=True,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
	legA.get_frame().set_alpha(0.65)

if small_flag:
	axB.set_xlabel(r'$t$')
	axB.set_ylabel(r'$MSD$')
	axB.set_xscale('log')
	axB.set_yscale('log')
	axB.set_xlim(np.min(t),np.max(t))
	#print "Number of lines in plot 1 = %d" % (len(ax1.get_lines()))
	legB = axB.legend(fancybox=True,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
	legB.get_frame().set_alpha(0.65)

#name = fileListName[fileListName.rfind('/')+1:]
fig2.savefig('results/'+name+'.DvskT.pdf')
figA.savefig('results/'+name+'.AMSD.png')
figB.savefig('results/'+name+'.BMSD.png')

if savedat_flag:
	np.savetxt('results/'+name+'.DvskT.txt',np.array(data).T)
