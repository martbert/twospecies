#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['lines.markersize'] = 6
mpl.rcParams['lines.markeredgewidth'] = 1.0
mpl.rcParams['figure.figsize'] = (5,4)
mpl.rcParams['figure.subplot.bottom'] = 0.15
mpl.rcParams['font.size'] = 14.0
#mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 1.0
mpl.rcParams['legend.fontsize'] = 14.0
mpl.rcParams['figure.subplot.right'] = 0.95
mpl.rcParams['figure.subplot.left'] = 0.20
mpl.rcParams['figure.subplot.top'] = 0.95
mpl.rcParams['axes.color_cycle'] = ['#000000','#E69F00','#56B4E9','#009E73','#F0E442','#0072B2','#D55E00','#CC79A7']

def use_latex():
    mpl.rcParams['text.usetex'] = True

