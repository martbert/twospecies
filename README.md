# Setu and run simulations related to attractive hard spheres

## Requirements

- HOOMD 0.11.2 (or above in the near future)
- custom_pair_potentials_plugin for HOOMD
- Python 2.7
- MDAnalysis module

## Step 1: create a system

Generate an XML file for a system to be simulation using **systemSetup.py**. Issue the following command in the root folder of this project:

    python systemSetup.py -f <FILENAME_PREFIX> -p <PROPORTION_OF_SMALL_PARTS> -d <POLYDISPERSITY>

This will create a system with at most 1000 big particles of diameter 32 \sigma with **-p 0.0** or 8000 small particles of diameter 16 \sigma with **-p 1.0**. Anything in between goes. These are setup is a very large box at low volume fraction. The particles sizes are sample from a uniform distribution with d_NOM +/- <POLYDISPERSITY>.

## Step 2: warm up the system

On a computer with HOOMD install and a decent GPU, run the following:

    hoomd twoSpecies.warm.hoomd.py --user="--f <FILENAME> --n 10000000 --t 1.0"

to bring the system to a total volumen fraction of 0.3 at temperature kT=1.0. The volume fraction can be change within the script **twoSpecies.warm.hoomd.py**. The temperature of 1.0 can also be modified although it can later done in the production run.

This will output a XML file with the name:

    <FILENAME>.go.xml

## Step 3: production run

On the smae machine, one can then start a production run by calling:

    hoomd twoSpecies.RampT2.hoomd.py --user="--f <FILENAME_PREFIX>.go --n 20000000"

In the present state of affiars, this will run a simulation that will slowly ramp down the temperature and stop at target values to explore the dynamics:

    kT = [0.8,0.77,0.74,0.71,0.68,0.65,0.62,0.59,0.56,0.53,0.5]

Make sure that the data_path in **twoSpecies.RampT2.hoomd.py** points to an appropriate place.

## Step 4: analyze

In the <DATA_PATH> under a folder named <FILENAME>.go there will be a bunch of files:
    - .T<TEMP>.msd files which contain the mean-square displacements for diffusion calculations
    - .T<TEMP>_main.mol2 with associated 



