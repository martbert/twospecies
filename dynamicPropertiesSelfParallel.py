#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
import numpy.fft as fft
import histogram
from scipy import optimize

import matplotlib
matplotlib.use('MacOSX')
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import pylab as pl
import matplotlib_params

import MDAnalysis as md
import MDAnalysis.analysis.distances as mddist
#from MDAnalysis.analysis.distances import self_distance_array,distance_array

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:p:s")
except getopt.GetoptError:
	usage()
	sys.exit(2)

self_flag = 0

for o, val in opts:
	if o == "-f":
		f = val
	if o == "-s":
		self_flag = 1
	if o == "-p":
		# Parallel environment is setup
		ncpus = int(val)
		# Import Parallel Python library
		import pp
		# Initialize job server
		ppservers = ()
		job_server = pp.Server(ncpus,ppservers=ppservers)
		print "Starting pp with", job_server.get_ncpus(), "workers"

############################################################
# Routines
############################################################

#class for callbacks
class Sum:
    def __init__(self,value):
        self.value = value
        self.lock = thread.allocate_lock()
        self.count = 0

    #the callback function
    def add(self, lvals):
    	# The list contains: 1->vh, 2->dt
        self.count += 1
        self.lock.acquire()
        for key,val in lvals[0].iteritems():
        	self.value[key][:,int(lvals[1])] += val
        self.lock.release()

def fftTrans(r,g,pad):
	n = g.size+pad
	dr = r[1]-r[0]
	# Transform
	f = 4.*np.pi*fft.fftshift(fft.fft(r*g,n=n))
	f = -np.imag(f)
	# Get frequencies
	q = 2.*np.pi*fft.fftshift(fft.fftfreq(n,d=dr))
	indices = np.where(q > 0)[0]
	return q[indices],f[indices]/q[indices]

def formFactor(q,R):
	vol = 4.*np.pi*R**3./3.
	factor = 3.*(np.sin(q*R)-q*R*np.cos(q*R))/(q*R)**3.
	return vol*factor

def calcSelfVanHove(fname,trajectories,delta,index,flags,indices,nbins,drange,rad):
	system = md.Universe(fname+'.mol2',fname+'.dcd')
	npts = len(system.atoms)
	box = system.dimensions[:3]
	boxvolume = np.prod(box) 
	dist = np.zeros((npts,npts),dtype=np.float64)

	vh = {}

	if flags[0]:
		vh['AA'] = np.zeros(nbins,dtype=np.float64)
	if flags[1]:
		vh['BB'] = np.zeros(nbins,dtype=np.float64)

	dmin,dmax = drange
	for i in trajectories: 
		itraj = system.trajectory[i]
		posi = system.atoms.coordinates()
		jtraj = system.trajectory[i+delta]
		posj = system.atoms.coordinates()
		mddist.distance_array(posi,posj,box,result=dist)
		if flags[0]:
			count = histogram.hist(dist[indices[0]], vh['AA'], nbins, dmin, dmax)
		if flags[1]:
			count = histogram.hist(dist[indices[1]], vh['BB'], nbins, dmin, dmax)

	# Transform in probability density (normalize)
	temp, edges = np.histogram([0], bins=nbins, range=(dmin, dmax))
	radii = 0.5*(edges[1:] + edges[:-1])
	dr = edges[1]-edges[0]
	vol = (4./3.)*np.pi*(np.power(edges[1:],3)-np.power(edges[:-1], 3))
	#vol = 4.*np.pi*radii**2.*dr
	if flags[0]:
		norm = np.sum(vh['AA'])
		vh['AA'] /= norm*vol
	if flags[1]:
		norm = np.sum(vh['BB'])
		vh['BB'] /= norm*vol

	return [vh,index]

def calcWrapArray(pos,box):
	pass

############################################################
# Main body
############################################################

# radii
rbig = 16.
rsmall = 8.

# Load system
try:
	system = md.Universe(f+'.mol2',f+'.dcd')
except:
	system = md.Universe(f+'.xml',format='DYMD')
	system.load_new(f+'.dcd')
# Define a group for all atoms
#gall = system.selectAtoms('resname A or resname B')
ntraj = len(system.trajectory)
box = system.dimensions[:3]
boxvolume = np.prod(box) 
boxh = 0.5*box
npts = len(system.atoms)
rho = npts /boxvolume

# Set limits
dmin, dmax = 0.0, 40.0
#dmin, dmax = 0.0, 30.0
nbins = 1000
pad = 2*nbins

maxtraj = 1000
#ts = np.unique(np.logspace(0.,3.,num=32).astype(int))
ts = np.unique(np.logspace(0.,4.,num=96).astype(int))
tmax = len(ts)

# Construct indices tuples
types = np.matrix(np.where(np.array([system.atoms[i].resname for i in range(len(system.atoms))]) == 'A',1,2))
pairMatrix = np.array(types.T * types)
# All pairs off diagonal
ident = np.eye(pairMatrix.shape[0])-1
ident *= -1
tempMatrix = ident*pairMatrix
iAA = np.where( tempMatrix == 1 )
iBB = np.where( tempMatrix == 4 )
iAB = np.where( tempMatrix == 2 )
# All pairs on diagonal
ident = np.eye(pairMatrix.shape[0])
tempMatrix = ident*pairMatrix
iA = np.where( tempMatrix == 1 )
iB = np.where( tempMatrix == 4 )
print "Indices matrix constructed"

# Set flag
if len(iAA[0]) > 0:
	AA_flag = True
	gA = system.selectAtoms('resname A')
	nA = len(gA)
	rhoA = nA / boxvolume
else:
	AA_flag = False
if len(iBB[0]) > 0:
	BB_flag = True
	gB = system.selectAtoms('resname B')
	nB = len(gB)
	rhoB = nB / boxvolume
else:
	BB_flag = False

# set up rdf calculation
rdfs = {}

if AA_flag:
	rdfs['AA'] = np.zeros((nbins,tmax),dtype=np.float64)
else:
	rdfs['AA'] = np.array([0])
if BB_flag:
	rdfs['BB'] = np.zeros((nbins,tmax),dtype=np.float64)
else:
	rdfs['BB'] = np.array([0])
new_rdf, edges = np.histogram([0],bins=nbins, range=(dmin, dmax))
radii = 0.5*(edges[1:] + edges[:-1])

# Prepare for parallel execution
dtcounts = np.zeros(tmax,dtype=int)
dist = np.zeros((npts,npts),dtype=np.float64)

# Create instance of callback class
start = time.time()
sumRdfs = Sum(rdfs)

flags = [AA_flag,BB_flag]
indices = [iAA,iBB,iAB]
indicesDiag = [iA,iB]
drange = [dmin,dmax]

jobs = []
maxtraj = min(maxtraj,ntraj-tmax)
trajectories = range(maxtraj)
for i in range(len(ts)):
	dt = ts[i]
	jobs.append(job_server.submit(calcSelfVanHove,args=(f,trajectories,dt,i,flags,indicesDiag,nbins,drange,[rbig,rsmall]),modules=("numpy as np","MDAnalysis as md","MDAnalysis.analysis.distances as mddist","histogram"),callback=sumRdfs.add))
print "Jobs submitted now waiting for Van Hove calculation ..."

# Wait for all jobs to complete
job_server.wait()
job_server.destroy()
end = time.time()
print "Time = %f" % (end-start)
print "All jobs are done."
rdfs = sumRdfs.value

# Fourier transform to get the partial structure factors
sqs = {}
q = 2.*np.pi*fft.fftshift(fft.fftfreq(nbins+pad,d=radii[1]-radii[0]))
js = np.where(q > 0)[0]
q = q[js]
nqs = len(q)
if AA_flag:
	sqs['AA'] = np.zeros((nqs,tmax),dtype=np.float64)
else:
	sqs['AA'] = np.array([0])
if BB_flag:
	sqs['BB'] = np.zeros((nqs,tmax),dtype=np.float64)
else:
	sqs['BB'] = np.array([0])

print "Calculating space Fourier transform of Van Hove function ..."
if AA_flag:
	ffA = formFactor(q/2./np.pi,rbig)
if BB_flag:
	ffB = formFactor(q/2./np.pi,rsmall)
#fig1 = pl.figure(1)
#ax1 = fig1.add_subplot(111)
#fig2 = pl.figure(2)
#ax2 = fig2.add_subplot(111)
for dt in range(tmax):
	if AA_flag:
		q,sq = fftTrans(radii,rdfs['AA'][:,dt],pad)
		sqs['AA'][:,dt] = sq
		#ax1.plot(radii,rdfs['AA'][:,dt],'-b',lw=1.0,alpha=0.7)
		#ax2.plot(q,sqs['AA'][:,dt],'-b',lw=1.0,alpha=0.7)
	if BB_flag:
		q,sq = fftTrans(radii,rdfs['BB'][:,dt],pad)
		sqs['BB'][:,dt] = sq
		#ax1.plot(radii,rdfs['BB'][:,dt],'-r',lw=1.0,alpha=0.7)
		#ax2.plot(q,sqs['BB'][:,dt],'-r',lw=1.0,alpha=0.7)

oname = f[f.rfind('/')+1:]
np.savetxt(oname+'.rs.txt',radii)
np.savetxt(oname+'.qs.txt',q)
if AA_flag:
	np.savetxt(oname+'.AA.gsrt.txt',rdfs['AA'])
	np.savetxt(oname+'.AA.fskt.txt',sqs['AA'])
if BB_flag:
	np.savetxt(oname+'.BB.gsrt.txt',rdfs['BB'])
	np.savetxt(oname+'.BB.fskt.txt',sqs['BB'])

#pl.show()
raw_input('Press <ENTER> to continue') 