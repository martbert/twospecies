#############################################################
# Import libraries
#############################################################

from hoomd_script import *
from hoomd_plugins import custom_pair_potentials as custpair
from optparse import OptionParser, OptionGroup
import math,random
import sys,os,re

#import numpy as np

#############################################################
# Extending the parser with custom parsing arguments parser definition
# This only works with a custom version implemented by MB
#############################################################
try:
	custom_parser = OptionGroup(globals.parser, "Custom Options","This group contains all user defined options.")
	custom_parser.add_option("-f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("-n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("-m", type="float", dest="rmin", help="Custom option: minimum distance")
	custom_parser.add_option("-t", type="float", dest="temp", help="Custom option: temperature")

	globals.parser.add_option_group(custom_parser)

	# The command line parser needs to be manually called (allows greater flexibility)
	init.parse_command_line()

	if globals.options.filename:
		name = globals.options.filename
	else:
		globals.parser.error("Need a filename to execute simulation")

	if globals.options.int_steps:
		int_steps = globals.options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if globals.options.rmin:
		rmin = globals.options.rmin
	else:
		rmin = -1.0

	if globals.options.temp:
		kT = globals.options.temp
	else:
		kT = 1.0

except:
	custom_parser = OptionParser()
	custom_parser.add_option("--f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("--n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("--m", type="float", dest="rmin", help="Custom option: minimum distance")
	custom_parser.add_option("--t", type="float", dest="temp", help="Custom option: temperature")

	custom_args = option.get_user()
	(options, args) = custom_parser.parse_args(custom_args)

	if options.filename:
		name = options.filename
	else:
		parser.error("Need a filename to execute simulation")

	if options.int_steps:
		int_steps = options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if options.rmin:
		rmin = options.rmin
	else:
		rmin = -1.0

	if options.temp:
		kT = options.temp
	else:
		kT = 1.0

#############################################################
# Routines
#############################################################

# Tabulated LJ potential with force cap
def tablj(r, rmin, rmax, epsilon, sigma, delta, cap):
     V = 4 * epsilon * ( (sigma / (r-delta))**12 - (sigma / (r-delta))**6);
     F = 4 * epsilon / (r-delta) * ( 12 * (sigma / (r-delta))**12 - 6 * (sigma / (r-delta))**6);
     F = min(F,cap)
     return (V, F)
	
#############################################################
# Main
#############################################################

# Some flags for the script
#############################################################
#Simulation flags
restart_flag = 0
overlap_flag = 1
slj_flag = 1

# Set some values
#############################################################
overlap_steps = 1000
overlap_runs = 24
warmup_steps = 10000
warmup_times = 1

pi = 3.14159265

# Parameters of the simulation
#############################################################=

#rc = 2.**(1./6.)
# Attractive part also
rc = 3.5
dt = 0.01

dbig = 32.0
dsmall = 16.0

# Initialize system
#############################################################=
# Data output path
#data_path = '/scratch/mbertram/'
#data_path = '/home/martbert/Documents/depletion/data/'
#data_path = '/Users/martinbertrand/Documents/Recherche/twoSpecies/data/'
data_path = '/raid/martin/data/twoSpecies/'
dir_path = data_path+name
# Verify if dir path exists, if not create
if not os.path.exists(dir_path):
	os.mkdir(dir_path)
fname = dir_path+'/'+name+'.T'+str(kT)

# Check wether a binary file of the system exists to continue simulation
file1 = fname+'.1.bin.gz'
file2 = fname+'.2.bin.gz'
file1_exists = os.path.exists(file1)
file2_exists = os.path.exists(file2)
if file1_exists and file2_exists:
	restart_flag = 1
	# Compare the dates and choose the most recent
	stat1 = os.stat(file1)
	stat2 = os.stat(file2)
	if stat1.st_atime > stat2.st_atime:
		system = init.read_bin(filename=file1)
		print 'Starting from restart file 1'
	else:
		system = init.read_bin(filename=file2)
		print 'Starting from restart file 2'
elif file1_exists:
	restart_flag = 1
	# Initialize with file1
	system = init.read_bin(filename=file1)
	print 'Starting from restart file 1'
elif file2_exists:
	restart_flag = 1
	# Initialize with file2 (this should not happen in theory)
	system = init.read_bin(filename=file2)
	print 'Starting from restart file 2'
else:
	# A new system is being setup
	system = init.read_xml(filename=name+'.xml')

# Box dimension
box = system.box

# Setup interactions
#############################################################

# Non-bonded interactions
#####################
# The particle types are:
bigtype = 'A'
smalltype = 'B'

#slj = pair.slj(r_cut=rc, d_max = dbig)
#slj.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, sigma=1.0)

if 1:
	syukawa = custpair.pair.syukawa(r_cut=rc, d_max = dbig)
	syukawa.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, kappa=10.0)

	egauss = -5.*(1.-kT)
	sgauss = custpair.pair.sgauss(r_cut=rc, d_max = dbig)
	sgauss.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=egauss, sigma=0.5)
else:
	lj = pair.lj(r_cut=1.12246)
	lj.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=0.0, sigma=1.0)

# Declare useful groups
#############################################################
# Polymer particles
gbig = group.type(name='big',type=bigtype)
gsmall = group.type(name='small',type=smalltype)
gall = group.all()
	
# Output a mol2 snapshot
mol2 = dump.mol2()
mol2.write(filename=fname+'_init.mol2')

# Reset exclusions
if 1:
	nlist.reset_exclusions(exclusions = ['diameter'])
	nlist.set_params(d_max = dbig)

# Warmup integration (only if it's not a restart)
#############################################################

standard = integrate.mode_standard(dt=dt)
zeroer = update.zero_momentum(period=1E7)

nvt = integrate.bdnvt(group=all,T=kT,gamma_diam=True)
nvt = integrate.bdnvt(group=gall,T=kT)
nvt.set_gamma(bigtype,gamma=dbig)
nvt.set_gamma(smalltype,gamma=dsmall)
#nvt = integrate.nvt(group=gall,tau=1.0,T=kT)

# Restart binary every 5e4
bin_restart = dump.bin(file1=file1, file2=file2, period=5e4)

#run(1E6)

# Main integration
#############################################################

# Analyzers
logger = analyze.log(filename=fname+'.log', period=1000,quantities=['time','kinetic_energy','potential_energy','pressure'], header_prefix='#')
msd = analyze.msd(groups=[gbig, gsmall], period=100, filename=fname+'.msd.log', header_prefix='#')

# Dump
mol2.write(filename=fname+'_main.mol2')
dump.dcd(filename=fname+'_main.dcd', group=gall, period=2500, unwrap_full=False)

# Run
for i in range(int_times):	
	run(int_steps)

# Force a last binary dump of system
# Compare the dates and choose the oldest
stat1 = os.stat(file1)
stat2 = os.stat(file2)
if stat1.st_atime > stat2.st_atime:
	bin_restart.write(filename=file2)
else:
	bin_restart.write(filename=file1)
