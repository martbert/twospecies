import numpy as np

def write_xml(fname=None,box=None,types=None,positions=None,masses=None,diameters=None,charges=None,bonds=None,angles=None,bodies=None,walls=None,dim=3):
		
	debug_flag = 0
	boxh = 0.5*box
	
	# Open output stream
	xmlfile = open(fname,'w')
	
	# Header
	if dim != 2:
		header = '<?xml version="1.0" encoding="UTF-8"?>\n<hoomd_xml version="1.3">\n<configuration time_step="0">\n\n'
	else:
		header = '<?xml version="1.0" encoding="UTF-8"?>\n<hoomd_xml version="1.3">\n<configuration time_step="0" dimensions="2">\n\n'

	# Create all necessary entries for the xml file
	position = '<position>\n'
	type = '<type>\n'
	if charges != None:
		charge = '<charge>\n'
	if masses != None:
		mass = '<mass>\n'
	if bonds != None:
		bond = '<bond>\n'
	if angles != None:
		angle = '<angle>\n'
	if diameters != None:
		diameter = '<diameter>\n'
	if bodies != None:
		body = '<body>\n'
	
	# Box dimension
	boxstr = '<box lx="%f" ly="%f" lz="%f"/>\n\n' % (box[0],box[1],box[2])
	if debug_flag: print boxstr
	
	xmlfile.write(header)
	xmlfile.write(boxstr)

	# Loop particles and append values to modules
	# wrap particles if necessary
	factors = (positions/boxh).astype(int)
	positions -= factors*box
	for i in range(len(positions)):
		
		# Type
		type += "%s\n" % (types[i])
		# Position
		position += "%f %f %f\n" % (positions[i,0],positions[i,1],positions[i,2])
		# Mass
		if masses != None:
			mass += "%f\n" % (masses[i])
		# Charge
		if charges != None:
			charge += "%f\n" % (charges[i])
		# Diameter
		if diameters != None:
			diameter += "%f\n" % (diameters[i])
		# Body
		if bodies != None:
			body += "%d\n" % (bodies[i])
	if debug_flag: print "Positions, masses, charges, and types have been printed"
	
	# Bond
	if bonds != None:
		for val in bonds:
			bond += "%s %d %d\n" % (val[0],val[1],val[2])
				
		if debug_flag: print "Bonds printed"
		
	# Angle
	if angles != None:
		for val in angles:
			angle += "%s %d %d %d\n" % (val[0],val[1],val[2],val[3])
	#	for key,val in angles.iteritems():
	#		angle += "%s" % (key)
	#		for param in val:
	#			angle += "%f" % (param)
	#		angle += "\n"
		if debug_flag: print "Angles printed"
		
	# Add footers
	position = position+'</position>\n\n'
	type = type+'</type>\n\n'
	xmlfile.write(position)
	xmlfile.write(type)
	
	if masses != None:
		mass = mass+'</mass>\n\n'
		xmlfile.write(mass)
	if charges != None:
		charge = charge+'</charge>\n\n'
		xmlfile.write(charge)
	if diameters != None:
		diameter = diameter+'</diameter>\n\n'
		xmlfile.write(diameter)
	if bodies != None:
		body = body+'</body>\n\n'
		xmlfile.write(body)
	if bonds != None:
		bond = bond+'</bond>\n\n'
		xmlfile.write(bond)
		if debug_flag: print "Bond footer added"
	if angles != None:
		angle = angle+'</angle>\n\n'
		xmlfile.write(angle)
		if debug_flag: print "Angle footer added"

	if walls != None:
		xmlfile.write(walls)

	footer = '</configuration>\n</hoomd_xml>\n'
	xmlfile.write(footer)
	if debug_flag: print "Footers are printed"
	
	xmlfile.close()	