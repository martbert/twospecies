#!/usr/bin/env python

import sys, getopt, os
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize
import re, pickle

import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b

def parseResults(fname):
	f = open(fname,'r')
	results = pickle.load(f)
	f.close()
	Fkt={}
	Grt={}
	for res in results:
		name = res[1]
		if name == 'k':
			ks = res[0][1:]
		elif name == 'r':
			rs = res[0][1:]
		elif name == 't':
			ts = res[0]
		elif name.find('F_k_t') != -1:
			i = name.find('F_k_t')+6
			types = tuple(name[i:].split('_'))
			Fkt[types] = res[0][0,1:]
		elif name.find('G_r_t') != -1:
			i = name.find('G_r_t')+6
			types = tuple(name[i:].split('_'))
			Grt[types] = res[0][0,1:]
		else:
			pass
	return rs,ks,ts,Fkt,Grt

############################################################
# Main body
############################################################

# Load list of filenames
fnames = np.loadtxt(fileList,dtype=str)
try:
	nfiles = len(fnames)
except:
	fnames = [str(fnames)]
	nfiles = len(fnames)

# Prepare to plot all
fig1 = pl.figure(1)
ax1 = fig1.add_subplot(111)

types = {'0':'A','1':'B'}

# Loop over the files
for i in range(nfiles):
	fname = fnames[i]
	# Get number of big and small
	name = fname[fname.rfind('/')+1:]
	print name
	sysvars = np.array(re.findall(r"[-+]?\d*\.\d+|\d+", name)).astype(float)
	big = sysvars[-3]
	small = sysvars[-2]
	#big = 500
	#small = 2700
	# Load data
	rs,ks,ts,Fkt,Grt = parseResults(fname)
	# Plot data
	for k,v in Fkt.iteritems():
		p1, = ax1.plot(ks/32.,v,label=r'Types: %s,%s' % (types[k[0]],types[k[1]]))
	
# Plot the results
ax1.set_xlabel(r'$q$')
ax1.set_ylabel(r'$S(q)$')
#ax1.set_ylim(0.,3.0)
leg1 = ax1.legend(loc='best',fancybox=True)
leg1.get_frame().set_alpha(0.65)

name = fileList[fileList.rfind('/')+1:]

pl.show()
fig1.savefig('results/'+name+'.Sq.pdf')
