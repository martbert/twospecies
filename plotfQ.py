#!/usr/bin/env python

import sys, getopt, os
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize
import re, pickle

import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b
fsexp = lambda x,a,b,c,d: 1.+a*(b*np.exp(-(x/c)**d))**2.

############################################################
# Main body
############################################################

# Load list of filenames
fnames = np.loadtxt(fileList,dtype=str)
try:
	nfiles = len(fnames)
except:
	fnames = [str(fnames)]
	nfiles = len(fnames)

# Prepare to plot all
fig1 = pl.figure(1)
ax1 = fig1.add_subplot(111)
# Loop over the files, load the data and find the diffusion coefficient
for i in range(nfiles):
	fname = fnames[i]
	# Get number of big and small
	name = fname[fname.rfind('/')+1:]
	print name
	sysvars = np.array(re.findall(r"[-+]?\d*\.\d+|\d+", name)).astype(float)
	big = sysvars[-3]
	small = sysvars[-2]
	# Load data
	f = open(fname,'r')
	res = pickle.load(f)
	f.close()
	# Assign variable
	k = res[0][0]
	t = res[1][0]
	Fkt = res[3][0]
	r = res[4][0]
	Grt = res[5][0]
	# Calculate the non-ergodicity parameters and plot
	nk = len(k)-1
	# Prepare data
	fQ = np.zeros(nk)
	for i in range(nk):
		Ft = Fkt[:,1+i]
		# Fit
		p,c = optimize.curve_fit(fsexp,t,Ft,p0=[0.3,1.0,100,1.1])
		fQ[i] = p[1]
	#p1, = ax1.plot(ks,Fks,label=r'$N_b=$%d, $N_s=$%d' % (big,small))
	p1, = ax1.plot(k[1:],fQ,label=r'$N_b=$%d, $N_s=$%d' % (big,small))
	
# Plot the results
ax1.set_xlabel(r'$q$')
ax1.set_ylabel(r'$f(q)$')
#ax1.set_ylim(0.,3.0)
leg1 = ax1.legend(loc='best',fancybox=True)
leg1.get_frame().set_alpha(0.65)
pl.show()

if 0:
	name = fileList[fileList.rfind('/')+1:]
	fig1.savefig('results/'+name+'.Sq.pdf')
