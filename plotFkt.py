#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
import numpy.fft as fft

import pylab as pl
import matplotlib_params
import matplotlib.cm as cm

def fftTrans(r,g,pad):
	n = g.size+pad
	dr = r[1]-r[0]
	# Transform
	f = 4.*np.pi*fft.fftshift(fft.fft(r*g,n=n))
	f = -np.imag(f)
	# Get frequencies
	q = 2.*np.pi*fft.fftshift(fft.fftfreq(n,d=dr))
	indices = np.where(q > 0)[0]
	return q[indices],f[indices]/q[indices]

def formFactor(q,R):
	vol = 4.*np.pi*R**3./3.
	factor = 3.*(np.sin(q*R)-q*R*np.cos(q*R))/(q*R)**3.
	return vol*factor

rbig = 16.
rsmall = 8.
rho = 7.8682635861307336e-05
nA = 500
nB = 4000
npts = 4500

nbins = 1000
pad = 3*nbins

AA_flag = BB_flag = AB_flag = 0
AA_flag = 1

fname = 'bigSmallK.b500.s4000.go.T0.60_main'

gaa = np.loadtxt(fname+'.AA.gdrt.txt')
gbb = np.loadtxt(fname+'.BB.gdrt.txt')
gab = np.loadtxt(fname+'.AB.gdrt.txt')
radii = np.loadtxt(fname+'.rd.txt')
ts = np.unique(np.logspace(0.,4.,num=96).astype(int))
nr,tmax = gaa.shape
tmax = 81
ts = ts[:tmax]
trans = np.where(gaa != 0,1,0)

# Fourier transform to get the partial structure factors
sqs = {}
q = 2.*np.pi*fft.fftshift(fft.fftfreq(nbins+pad,d=radii[1]-radii[0]))
js = np.where(q > 0)[0]
q = q[js]
nqs = len(q)
if AA_flag:
	sqs['AA'] = np.zeros((nqs,tmax),dtype=np.float64)
else:
	sqs['AA'] = np.array([0])
if BB_flag:
	sqs['BB'] = np.zeros((nqs,tmax),dtype=np.float64)
else:
	sqs['BB'] = np.array([0])
if AB_flag:
	sqs['AB'] = np.zeros((nqs,tmax),dtype=np.float64)
else:
	sqs['AB'] = np.array([0])

print "Calculating space Fourier transform of Van Hove function ..."
I = np.zeros((len(q),tmax),dtype=np.float64)
if AA_flag:
	ffA = formFactor(q/2./np.pi,rbig)
if BB_flag:
	ffB = formFactor(q/2./np.pi,rsmall)
fig1 = pl.figure(1)
ax1 = fig1.add_subplot(111)
fig2 = pl.figure(2)
ax2 = fig2.add_subplot(111)
fig3 = pl.figure(3)
ax3 = fig3.add_subplot(111)
for dt in range(tmax):
	if AA_flag:
		xA = nA/npts
		q,sq = fftTrans(radii,(gaa[:,dt]-1.),pad)
		sq = xA + rho*xA*xA*sq
		sqs['AA'][:,dt] = sq
		I[:,dt] += ffA**2.*sq
		ax1.plot(radii,gaa[:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)
		ax2.plot(q,sqs['AA'][:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)
	if BB_flag:
		xB = nB/npts
		q,sq = fftTrans(radii,(gbb[:,dt]-1.),pad)
		sq = xB + rho*xB*xB*sq
		sqs['BB'][:,dt] = sq
		I[:,dt] += ffB**2.*sq
		ax1.plot(radii,gbb[:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)
		ax2.plot(q,sqs['BB'][:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)
	if AB_flag:
		q,sq = fftTrans(radii,(gab[:,dt]-1.),pad)
		sq = rho*xA*xB*sq
		sqs['AB'][:,dt] = sq
		I[:,dt] += 2.*ffA*ffB*sq
		ax1.plot(radii,gab[:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)
		ax2.plot(q,sqs['BB'][:,dt],'-',lw=1.0,color=cm.autumn(dt/tmax),alpha=0.7)

print q[25]
#ax3.semilogx(ts,I[14,:]/I[14,0],'-',lw=1.0,color='red',alpha=0.7)

for qq in range(0,25,1):
	ax3.semilogx(ts,sqs['AA'][qq,:]/sqs['AA'][qq,0],'-',lw=1.0,color=cm.autumn(qq/25),alpha=0.7)

pl.show()
#raw_input('Press <ENTER> to continue') 