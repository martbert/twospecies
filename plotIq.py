#!/usr/bin/env python

import sys, getopt, os
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize
import re, pickle

import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b

############################################################
# Main body
############################################################

# Load list of filenames
fnames = np.loadtxt(fileList,dtype=str)
try:
	nfiles = len(fnames)
except:
	fnames = [str(fnames)]
	nfiles = len(fnames)

# Prepare to plot all
fig1 = pl.figure(1)
ax1 = fig1.add_subplot(111)

# Loop over the files, load the data and find the diffusion coefficient
for i in range(nfiles):
	fname = fnames[i]
	# Get number of big and small
	name = fname[fname.rfind('/')+1:]
	print name
	sysvars = np.array(re.findall(r"[-+]?\d*\.\d+|\d+", name)).astype(float)
	#big = sysvars[-3]
	#small = sysvars[-2]
	kT = sysvars[-1]
	#big = 300
	#small = 2700
	# Load data
	d = np.loadtxt(fname)
	# Assign variable
	q = d[:,0]
	Iq = d[:,1]
	p1, = ax1.plot(q,Iq,lw=2.0,label=r'$kT=$%.1f' % (kT))
	
# Plot the results
ax1.set_xlabel(r'$q$')
ax1.set_ylabel(r'$I(q)$')
ax1.set_ylim(-4.5E7,1.38E8)
ax1.set_xlim(0.1,0.5)
leg1 = ax1.legend(loc='best',fancybox=True)
leg1.get_frame().set_alpha(0.65)

name = fileList[fileList.rfind('/')+1:]
fig1.savefig('results/'+name+'.Iq.pdf')
pl.show()
