#!/usr/bin/env python

import sys, getopt, os
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize

import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		f = val

############################################################
# Routines
############################################################

############################################################
# Main body
############################################################

f = f.split(',')

for fn in f:
	x,y = np.loadtxt(fn,unpack=True,skiprows=1)
	indices = np.argsort(x)
	pl.plot(x[indices],y[indices],'-',lw=2.0,label=fn) 
pl.legend(loc='best')
pl.xlabel(r'$q$')
pl.ylabel(r'$I(q)$')
pl.show()