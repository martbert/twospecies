#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
from scipy import optimize
from scipy.special import sph_harm

import pylab as pl
import seaborn as sb

import MDAnalysis as md
import MDAnalysis.analysis.distances as mddist

# For saving the data
import h5py

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv,"f:p:s")
except getopt.GetoptError:
    usage()
    sys.exit(2)

self_flag = 0

for o, val in opts:
    if o == "-f":
        f = val
    if o == "-s":
        self_flag = 1
    if o == "-p":
        # Parallel environment is setup
        ncpus = int(val)
        # Import Parallel Python library
        import pp
        # Initialize job server
        ppservers = ()
        job_server = pp.Server(ncpus,ppservers=ppservers)
        print "Starting pp with", job_server.get_ncpus(), "workers"

############################################################
# Routines
############################################################

def calc_Q6locFrame (system, group, idx, cutoff=12.0, ct_mtx=None, nI=None):
    # Traj
    traj = system.trajectory[idx]

    # Box
    box = system.dimensions[:3]
    
    # Coords (wrapped in the box)
    coord = np.remainder(group.coordinates(), box)

    # Calculate contact matrix for the group specified at the specified frame
    if ct_mtx is not None:
        mddist.distance_array(coord, coord, box, result=ct_mtx)
    else:
        ct_mtx = mddist.distance_array(coord, coord, box)
    if nI is None:
        nI = (np.eye(len(group), dtype=bool) != True)
    ct_mtx = (ct_mtx < cutoff) & nI

    # Simply loop over the particles to calculate the Q_{6,local} value for each
    Q6local = np.zeros(len(group))
    for i in range(len(group)):
        Q = 0

        if ct_mtx[i].sum() > 0:
            pts = coord[ct_mtx[i]]

            # Distances
            dx = pts - coord[i]
            r = np.sqrt(np.sum(dx**2,axis=1))
            
            # Angles
            phi = np.arctan2(dx[:,1], dx[:,0])
            theta = np.arccos(dx[:,2] / r)

            # Spherical harmonics
            for m in range(-6,7):
                ql = sph_harm(m,6,phi,theta).sum()
                ql /= len(r)
                ql = (ql*ql.conjugate()).astype(float)
                Q += ql

        Q6local[i] = Q

    # Return the Q_{6,local} factor
    Q6local = np.sqrt(4.0 * np.pi / 13.0 * Q6local)
    return Q6local

def calc_Q6locAverages(system, gA, gB, idx, Q, cutoff=30.0, ct_mtx=None):
    # Traj
    traj = system.trajectory[idx]

    # Box
    box = system.dimensions[:3]

    # Coords
    coordA = gA.coordinates()
    coordB = gB.coordinates()

    # Calculate contact matrix for the group specified at the specified frame
    if ct_mtx is not None:
        mddist.distance_array(coordA, coordB, box, result=ct_mtx)
    else:
        ct_mtx = mddist.distance_array(coordA, coordB, box)
    ct_mtx = (ct_mtx < cutoff)

    # Get indices where contact is True
    i,j = np.where(ct_mtx)

    # Unique indices of group B
    j = np.unique(j)

    # Q_{6,local} value for those in proximity to group A
    Q6local_prox = np.nanmean(Q[j])

    # Q_{6,local} value all over
    Q6local_all = np.nanmean(Q)

    return [Q6local_all, Q6local_prox]

def calc_CNumFrame (system, group, idx, cutoff=12.0, ct_mtx=None, nI=None):
    # Traj
    traj = system.trajectory[idx]

    # Box
    box = system.dimensions[:3]
    
    # Coords (wrapped in the box)
    coord = np.remainder(group.coordinates(), box)

    # Calculate contact matrix for the group specified at the specified frame
    if ct_mtx is not None:
        mddist.distance_array(coord, coord, box, result=ct_mtx)
    else:
        ct_mtx = mddist.distance_array(coord, coord, box)
    if nI is None:
        nI = (np.eye(len(group), dtype=bool) != True)
    ct_mtx = (ct_mtx < cutoff) & nI

    # Simply loop over the particles to calculate the Q_{6,local} value for each
    CNum = np.zeros(len(group))
    for i in range(len(group)):
        CNum[i] = ct_mtx[i].sum()

    return CNum

def calc_CNumAvg (system, gA, gB, idx, CNum, cutoff=30.0, ct_mtx=None):
    # Traj
    traj = system.trajectory[idx]

    # Box
    box = system.dimensions[:3]

    # Coords
    coordA = gA.coordinates()
    coordB = gB.coordinates()

    # Calculate contact matrix for the group specified at the specified frame
    if ct_mtx is not None:
        mddist.distance_array(coordA, coordB, box, result=ct_mtx)
    else:
        ct_mtx = mddist.distance_array(coordA, coordB, box)
    ct_mtx = (ct_mtx < cutoff)

    # Get indices where contact is True
    i,j = np.where(ct_mtx)

    # Unique indices of group B
    j = np.unique(j)

    # Q_{6,local} value for those in proximity to group A
    CNum_prox = np.mean(CNum[j])

    # Q_{6,local} value all over
    CNum_all = np.mean(CNum)

    return [CNum_all, CNum_prox]

############################################################
# Main body
############################################################

# radii
rbig = 16.
rsmall = 8.

# Load system
system = md.Universe(f+'.mol2',f+'.dcd')
# Define a group for all atoms
gall = system.selectAtoms('resname A or resname B')
ntraj = len(system.trajectory)
box = system.dimensions[:3]
boxvolume = np.prod(box) 
boxh = 0.5*box
npts = len(system.atoms)
rho = npts /boxvolume

gA = system.selectAtoms('resname A')
nA = len(gA)
rhoA = nA / boxvolume

gB = system.selectAtoms('resname B')
nB = len(gB)
rhoB = nB / boxvolume

if 1:
    # Loop through the number of frames and calculate the Q_{6,local} per bead

    # Open output hdf5 file
    hdf = h5py.File(f+'_Q.h5', 'w')

    # Compute average for all small beads in the vicinity of the bigs
    Q6local_prox = []

    # Compute average for all beads in the system
    Q6local_all = []

    # Pre-allocate memory for contact matrix and negative identity
    ct_mtx_BB = np.zeros((len(gB), len(gB)))
    ct_mtx_AB = np.zeros((len(gA), len(gB)))
    nI = (np.eye(len(gB), dtype=bool) != True)

    skip = 1000

    for itraj in range(0, ntraj, skip):
        # Calculate the Q6 factor 
        Q = calc_Q6locFrame(system, gB, itraj, cutoff=2.25 * rsmall, 
            ct_mtx=ct_mtx_BB, nI=nI)

        # Save to file
        hdf[str(itraj)] = Q

        # Calculate the average Q_{6,local} around the bigger colloids 
        # and in the bulk
        res = calc_Q6locAverages(system, gA, gB, itraj, Q, 
            cutoff=rbig + 1.2 * rsmall, ct_mtx=ct_mtx_AB)

        Q6local_all.append(res[0])
        Q6local_prox.append(res[1])

        print "Trajectory %d analyzed" % (itraj)

    # Save the averages
    data = np.array([Q6local_all,Q6local_prox])

    hdf["Q6avg"] = data

    hdf.close()

# Loop through the number of frames and calculate the Q_{6,local} per bead

# Open output hdf5 file
hdf = h5py.File(f+'_CN.h5', 'w')

# Compute average for all small beads in the vicinity of the bigs
CNum_prox = []

# Compute average for all beads in the system
CNum_all = []

# Pre-allocate memory for contact matrix and negative identity
ct_mtx_BB = np.zeros((len(gB), len(gB)))
ct_mtx_AB = np.zeros((len(gA), len(gB)))
nI = (np.eye(len(gB), dtype=bool) != True)

skip = 1000

for itraj in range(0, ntraj, skip):
    # Calculate the Q6 factor 
    Q = calc_CNumFrame(system, gB, itraj, cutoff=2.25 * rsmall, 
        ct_mtx=ct_mtx_BB, nI=nI)

    # Save to file
    hdf[str(itraj)] = Q

    # Calculate the average Q_{6,local} around the bigger colloids 
    # and in the bulk
    res = calc_CNumAvg(system, gA, gB, itraj, Q, 
        cutoff = rbig + 1.2 * rsmall, ct_mtx=ct_mtx_AB)

    CNum_all.append(res[0])
    CNum_prox.append(res[1])

    print "Trajectory %d analyzed" % (itraj)

# Save the averages
data = np.array([CNum_all,CNum_prox])

hdf["CNavg"] = data

hdf.close()

