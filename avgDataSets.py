#!/usr/bin/env python

import sys, os
from optparse import OptionParser, OptionGroup
import numpy as np

###############################################################
# Get command line arguments
###############################################################
parser = OptionParser()
# Filename
parser.add_option("-f", dest="fnames", help="List of comma seperated file names to be averaged together")
parser.add_option("-l", dest="listfiles", help="A file containing a list of files to be averaged")
parser.add_option("-o", dest="oname", help="Output file name")

(options, args) = parser.parse_args()

if options.fnames:
	fnames = list(options.fnames.split(','))
	nfiles = len(fnames)
elif options.listfiles:
	fnames = list(np.loadtxt(options.listfiles,dtype=str))
	nfiles = len(fnames)
else:	
	raise "Provide file names"

if options.oname:
	oname = options.oname
else:
	oname = "default.out.txt"

############################################################
# Routines
############################################################

############################################################
# Main body
############################################################

avgData = None

i = 0
for fname in fnames:
	data = np.loadtxt(fname)
	print fname
	print data.shape
	if i == 0:
		avgData = np.array(data)
	else:
		avgData += data
	i += 1
# Average
avgData /= nfiles

# Output
np.savetxt(oname,avgData)
