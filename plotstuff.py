#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
import histogram
from scipy import optimize

import numpy as np
import pylab as pl
import matplotlib.cm as cm

import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['lines.markersize'] = 6
mpl.rcParams['lines.markeredgewidth'] = 1.0
mpl.rcParams['figure.figsize'] = (5,4)
mpl.rcParams['figure.subplot.bottom'] = 0.15
mpl.rcParams['font.size'] = 14.0
#mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Times New Roman'
mpl.rcParams['axes.linewidth'] = 1.0
mpl.rcParams['legend.fontsize'] = 14.0
mpl.rcParams['figure.subplot.right'] = 0.8
mpl.rcParams['figure.subplot.left'] = 0.20
mpl.rcParams['figure.subplot.top'] = 0.95
mpl.rcParams['axes.color_cycle'] = ['#000000','#E69F00','#56B4E9','#009E73','#F0E442','#0072B2','#D55E00','#CC79A7']

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

self_flag = 0

for o, val in opts:
	if o == "-f":
		pfx = val

#pfx = 'results/bigSmallO.b500.s4000.go.T0.65_main'

############################################################
# Main body
############################################################

if 0:
	fig = pl.figure(1)
	ax = fig.add_subplot(111)

	p1, = ax.semilogy(b[:,0],b[:,1],'--o',lw=2,alpha=0.5,label=r'Pure big')
	p2, = ax.semilogy(s[:,0],s[:,1],'--o',lw=2,alpha=0.5,label=r'Pure small')
	p3, = ax.semilogy(bs[:,0],bs[:,1],'--o',lw=2,color=p1.get_color(),label=r'Mix big')
	p4, = ax.semilogy(bs[:,0],bs[:,2],'--o',lw=2,color=p2.get_color(),label=r'Mix small')

	ax.legend(loc='best')
	ax.set_xlabel(r'$kT$')
	ax.set_ylabel(r'$D$')

if 1:
	ga = np.loadtxt(pfx+'.AA.gsrt.txt')
	gb = np.loadtxt(pfx+'.BB.gsrt.txt')
	r = np.loadtxt(pfx+'.rs.txt')
	nr,nt = ga.shape


	fig = pl.figure(1)
	ax1 = fig.add_subplot(211)
	ax2 = fig.add_subplot(212)

	rmax = 10.
	indices = np.where(r < rmax)[0]
	dr = r[1]-r[0]
	dv = 4.*np.pi*r[indices]**2.*dr
	for t in range(nt):
		if t == 0:
			p1, = ax1.plot(r[indices],dv*ga[indices,t],'-',lw=2.0,color=cm.autumn(t/nt),alpha=0.7,label=r'Big')
			p2, = ax2.plot(r[indices],dv*gb[indices,t],'-',lw=2.0,color=cm.autumn(t/nt),alpha=0.7,label=r'Small')
			#c1 = p1.get_color()
			#c2 = p2.get_color()
		else:
			ax1.plot(r[indices],dv*ga[indices,t],'-',lw=2.0,color=cm.autumn(t/nt),alpha=0.7)
			ax2.plot(r[indices],dv*gb[indices,t],'-',lw=2.0,color=cm.autumn(t/nt),alpha=0.7)

	ax1.legend(loc='best')
	ax2.legend(loc='best')
	ax = fig.add_axes( [0., 0., 1, 1] )
	ax.set_axis_off()
	ax.set_xlim(0, 1)
	ax.set_ylim(0, 1)
	ax.text( 
	    .05, 0.5, r"$4\pi r^2G(r,t)$", rotation='vertical',
	    horizontalalignment='center', verticalalignment='center'
	)
	ax.text( 
	    .5, 0.05, r"$r$", rotation='horizontal',
	    horizontalalignment='center', verticalalignment='center'
	)
	fig.savefig(pfx+'mix.gsrt.pdf')

if 1:
	fa = np.loadtxt(pfx+'.AA.fskt.txt')
	fb = np.loadtxt(pfx+'.BB.fskt.txt')
	q = np.loadtxt(pfx+'.qs.txt')
	nq,nt = fa.shape
	pl.figure(3)
	pl.plot(q,fa[:,0])
	pl.show()

	fig = pl.figure(2)
	ax1 = fig.add_subplot(211)
	ax2 = fig.add_subplot(212)

	t = np.unique(np.logspace(0.,4.,num=96).astype(int))
	dt = t[1:]-t[:-1]
	np.savetxt(pfx+'.time.txt',t.T)
	qmin = 5
	qmax = 50
	qdelta = 5
	# Save the first
	data = np.array([t,fa[qmin,:]/fa[qmin,0],fb[qmin,:]/fb[qmin,0]]).T
	np.savetxt(pfx+'phi_lowq_t.txt',data)
	for i in range(qmin,qmax,qdelta):
		# Normalize
		na = np.sum(fa[i,:-1]*dt)
		nb = np.sum(fb[i,:-1]*dt)
		if i == qmin:
			ax1.semilogx(t,fa[i,:]/fa[i,0],'-',lw=2.0,color=cm.autumn(i/qmax),alpha=0.7,label='Big')
			ax2.semilogx(t,fb[i,:]/fb[i,0],'-',lw=2.0,color=cm.autumn(i/qmax),alpha=0.7,label='Small')
		else:
			ax1.semilogx(t,fa[i,:]/fa[i,0],'-',lw=2.0,color=cm.autumn(i/qmax),alpha=0.7)
			ax2.semilogx(t,fb[i,:]/fb[i,0],'-',lw=2.0,color=cm.autumn(i/qmax),alpha=0.7)
	
	cmap = cm.autumn
	norm = mpl.colors.Normalize(vmin=q[5], vmax=q[45])
	axcbar = fig.add_axes([0.85, 0.15, 0.05, 0.8])
	mpl.colorbar.ColorbarBase(axcbar, cmap=cmap,norm=norm,orientation='vertical')
	
	ax1.legend(loc='best')
	ax2.legend(loc='best')
	ax1.set_xlim(0,2000)
	ax2.set_xlim(0,2000)
	ax = fig.add_axes( [0., 0., 1, 1] )
	ax.set_axis_off()
	ax.set_xlim(0, 1)
	ax.set_ylim(0, 1)
	ax.text( 
	    .05, 0.5, r"$\phi(q,t)$", rotation='vertical',
	    horizontalalignment='center', verticalalignment='center'
	)
	ax.text( 
	    .5, 0.05, r"$t$", rotation='horizontal',
	    horizontalalignment='center', verticalalignment='center'
	)
	fig.savefig(pfx+'mix.fskt.pdf')

pl.show()
#ax1.set_xlabel(r'$r$')
#ax1.set_ylabel(r'$4\pi r^2G(r,t)$')