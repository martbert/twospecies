#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
from scipy import optimize

import pylab as pl
import seaborn as sb
sb.set(font_scale=1.4, style='ticks')

# For saving the data
import h5py

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv,"f:p:s")
except getopt.GetoptError:
    usage()
    sys.exit(2)

self_flag = 0

for o, val in opts:
    if o == "-f":
        f = val
    if o == "-s":
        self_flag = 1
    if o == "-p":
        # Parallel environment is setup
        ncpus = int(val)
        # Import Parallel Python library
        import pp
        # Initialize job server
        ppservers = ()
        job_server = pp.Server(ncpus,ppservers=ppservers)
        print "Starting pp with", job_server.get_ncpus(), "workers"

############################################################
# Routines
############################################################

############################################################
# Main
############################################################

files = ['/Volumes/ExternalHD/projects/twoSpecies/data/bigSmallBoth.b500.s4000.p0.03.go/bigSmallBoth.b500.s4000.p0.03.go.T0.71_main_Q.h5',
	'/Volumes/ExternalHD/projects/twoSpecies/data/bigSmallBoth.b500.s4000.p0.03.go/bigSmallBoth.b500.s4000.p0.03.go.T0.68_main_Q.h5',
	'/Volumes/ExternalHD/projects/twoSpecies/data/bigSmallBoth.b500.s4000.p0.03.go/bigSmallBoth.b500.s4000.p0.03.go.T0.65_main_Q.h5',
	'/Volumes/ExternalHD/projects/twoSpecies/data/bigSmallBoth.b500.s4000.p0.03.go/bigSmallBoth.b500.s4000.p0.03.go.T0.62_main_Q.h5'
]

T = ['0.71', '0.68', '0.65', '0.62']

dt = 0.005 * 500
ts = np.arange(0,40000,1000)

fig = pl.figure(1, figsize=[8,4])

for i in range(len(files)):
	# Open
	f = h5py.File(files[i], 'r')

	# Get data
	Ql, Qt = f['Q6avg'][...] / 0.575

	# Plot
	ax = fig.add_subplot(111)

	if i == 0:
		p1, = ax.plot(ts * dt, Ql, '-', label=r'Around bigger colloids')
		ax.plot(ts * dt, Qt, '--', color=p1.get_color(), label=r'Overall')
	else:
		p1, = ax.plot(ts * dt, Ql, '-')
		ax.plot(ts * dt, Qt, '--', color=p1.get_color())
	ax.plot(np.zeros(100) + ts[-1] * dt, 
		np.linspace(0,1,100),'--',color=p1.get_color())
	
	# Add temperature annotation
	ax.text(ts[5] * dt, 0.22 / 0.575, r'$kT=%s$' % (T[i]))

	ts += 40000

	ax.set_xlabel(r'$t$')
	ax.set_ylabel(r'$Q_6 / Q_6^{\mathrm{fcc}}$')
	ax.set_ylim(0,1)
	ax.legend(loc='best')

	f.close()

pl.tight_layout()
pl.show()