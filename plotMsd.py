#!/usr/bin/env python

import sys, getopt, os
import numpy as np
from scipy import optimize
import re

import matplotlib
#matplotlib.use('agg')
import pylab as pl
import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b
ftanh = lambda x,a,b,c: a*(np.tanh(b*(x+c))+1.)
dftanh = lambda x,a,b,c: a*b/np.cosh(b*(x+c))**2.
d2ftanh = lambda x,a,b,c: -2.*a*b**2./np.cosh(b*(-c + x))**2.*np.tanh(b*(-c + x))

############################################################
# Main body
############################################################

fig = pl.figure(1)
ax = fig.add_subplot(111)

# Load mix data and plot
mix = np.loadtxt('results/bigSmallOPQ.b500.s4000.msdlist.txt.DvskT.txt')
p1, = ax.plot(mix[:,0],mix[:,1],'--o',lw=2.0,ms=7,label='Big-Mix')
p2, = ax.plot(mix[:,0],mix[:,2],'--o',lw=2.0,ms=7,label='Small-Mix')

# Load big data and plot
big = np.loadtxt('results/bigSmallOPQ.b1000.s0.msdlist.txt.DvskT.txt')
ax.plot(big[:,0],big[:,1],'--s',lw=2.0,ms=7,color=p1.get_color(),alpha=0.5,label='Big-Pure')

# Load small data and plot
small = np.loadtxt('results/bigSmallOPQ.b0.s8000.msdlist.txt.DvskT.txt')
ax.plot(small[:,0],small[:,1],'--s',lw=2.0,ms=7,color=p2.get_color(),alpha=0.5,label='Small-Pure')

leg = ax.legend(loc='best',fancybox=True,framealpha=0.5)
ax.set_xlabel(r'$kT$')
ax.set_ylabel(r'$D$')

pl.show()