#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
import numpy.fft as fft
import histogram
from scipy import optimize

import matplotlib
matplotlib.use('MacOSX')
#matplotlib.use('agg')
import pylab as pl
import matplotlib_params

import MDAnalysis as md
import MDAnalysis.analysis.distances as mddist
#from MDAnalysis.analysis.distances import self_distance_array,distance_array

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:p:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		f = val
	if o == "-p":
		# Parallel environment is setup
		ncpus = int(val)
		# Import Parallel Python library
		import pp
		# Initialize job server
		ppservers = ()
		job_server = pp.Server(ncpus,ppservers=ppservers)
		print "Starting pp with", job_server.get_ncpus(), "workers"

############################################################
# Routines
############################################################

#class for callbacks
class Sum:
    def __init__(self,value,maxCount):
        self.value = value
        self.lock = thread.allocate_lock()
        self.count = 0

    #the callback function
    def add(self, dictofvalues):
        # we must use lock here because += is not atomic
        self.count += 1
        self.lock.acquire()
        for key,val in dictofvalues.iteritems():
        	self.value[key][1] += val
        self.lock.release()

def fftTrans(r,g,pad):
	n = g.size+pad
	dr = r[1]-r[0]
	# Transform
	f = 4.*np.pi*fft.fftshift(fft.fft(r*g,n=n))
	f = -np.imag(f)
	# Get frequencies
	q = 2.*np.pi*fft.fftshift(fft.fftfreq(n,d=dr))
	indices = np.where(q > 0)[0]
	return q[indices],f[indices]/q[indices]

def formFactor(q,R):
	vol = 4.*np.pi*R**3./3.
	factor = 3.*(np.sin(q*R)-q*R*np.cos(q*R))/(q*R)**3.
	return vol*factor

def calcRdfs(fname,trajectories,flags,indices,nbins,drange):
	system = md.Universe(fname+'.mol2',fname+'.dcd')
	npts = len(system.atoms)
	box = system.dimensions[:3]
	dist = np.zeros((npts*(npts-1)/2,), dtype=np.float64)
	tempHist = np.zeros(nbins,dtype=np.float64)
	
	rdf = {}
	if flags[0]:
		rdf['AA'] = np.zeros(nbins,dtype=np.float64)
		nAA = len(indices[0])
	if flags[1]:
		rdf['BB'] = np.zeros(nbins,dtype=np.float64)
		nBB = len(indices[1])
	if flags[2]:
		rdf['AB'] = np.zeros(nbins,dtype=np.float64)
		nAB = len(indices[2])

	dmin, dmax = drange
	for itraj in trajectories:
		traj = system.trajectory[itraj]
		pos = system.atoms.coordinates()
		mddist.self_distance_array(pos,box,dist)
		first_flag = 1
		if flags[0]:
			tempHist *= 0.0
			count = histogram.hist(dist[indices[0]], tempHist, nbins, dmin, dmax)
			rdf['AA'] += tempHist/count
		else:
			rdf['AA'] = 0
		if flags[1]:
			tempHist *= 0.0
			count = histogram.hist(dist[indices[1]], tempHist, nbins, dmin, dmax)
			rdf['BB'] += tempHist/count
		else:
			rdf['BB'] = 0
		if flags[2]:
			tempHist *= 0.0
			count = histogram.hist(dist[indices[2]], tempHist, nbins, dmin, dmax)
			rdf['AB'] += tempHist/count
		else:
			rdf['AB'] = 0
	return rdf

############################################################
# Main body
############################################################

# radii
rbig = 16.
rsmall = 8.

# Load system
try:
	system = md.Universe(f+'.mol2',f+'.dcd')
except:
	system = md.Universe(f+'.xml',format='DYMD')
	system.load_new(f+'.dcd')
# Define a group for all atoms
#ntraj = len(system.trajectory)
ntraj = 576
dtraj = 50
box = system.dimensions[:3]
boxvolume = np.prod(box)
boxh = 0.5*box
npts = len(system.atoms)

# Set limits
dmin, dmax = 0.0, 80.0
nbins = 1000

# Construct pairs table and range of indices
try:
	pairs = np.load(f+'.pairs.npy')
except:	
	pairs = []
	for i in xrange(npts):
		for j in xrange(i+1,npts):
			name1 = system.atoms[i].resname
			name2 = system.atoms[j].resname
			pairs.append(name1+name2)
	pairs = np.array(pairs,dtype=str)
	# Save pairs array for faster start up time
	np.save(f+'.pairs',pairs)
iAA = np.where(pairs == 'AA')[0]
iBB = np.where(pairs == 'BB')[0]
iAB = np.where(pairs == 'AB')[0]
iBA = np.where(pairs == 'BA')[0]
iAB = np.append(iAB,iBA)
print "Pair array constructed"

# Set flag
if len(iAA) > 0:
	AA_flag = True
	gA = system.selectAtoms('resname A')
	nA = len(gA)
else:
	AA_flag = False
if len(iBB) > 0:
	BB_flag = True
	gB = system.selectAtoms('resname B')
	nB = len(gB)
else:
	BB_flag = False
if len(iAB) > 0:
	AB_flag = True
else:
	AB_flag = False

# set up rdf calculation
rdfs = {}
dist = np.zeros((npts*(npts-1)/2,), dtype=np.float64)

if AA_flag:
	rdfAA, edges = np.histogram([0], bins=nbins, range=(dmin, dmax))
	rdfAA *= 0
	rdfAA = rdfAA.astype(np.float64)  # avoid possible problems with '/' later on
	radii = 0.5*(edges[1:] + edges[:-1])
	rdfs['AA'] = [radii,rdfAA]
else:
	rdfs['AA'] = [0,0]
if BB_flag:
	rdfBB, edges = np.histogram([0], bins=nbins, range=(dmin, dmax))
	rdfBB *= 0
	rdfBB = rdfBB.astype(np.float64)  # avoid possible problems with '/' later on
	radii = 0.5*(edges[1:] + edges[:-1])
	rdfs['BB'] = [radii,rdfBB]
else:
	rdfs['BB'] = [0,0]
if AB_flag:
	rdfAB, edges = np.histogram([0], bins=nbins, range=(dmin, dmax))
	rdfAB *= 0
	rdfAB = rdfAB.astype(np.float64)  # avoid possible problems with '/' later on
	radii = 0.5*(edges[1:] + edges[:-1])
	rdfs['AB'] = [radii,rdfAB]
else:
	rdfs['AB'] = [0,0]

# Prepare for parallel execution
# Create instance of callback class
start = time.time()
sumRdfs = Sum(rdfs,ntraj)

flags = [AA_flag,BB_flag,AB_flag]
indices = [iAA,iBB,iAB]
drange = [dmin,dmax]

jobs = []
numPerCpu = int(ntraj/ncpus)
count = 0
for i in range(ncpus):
	j = i+1
	lower = i*numPerCpu*dtraj
	upper = j*numPerCpu*dtraj
	if i == ncpus - 1:
		upper = max(upper,ntraj*dtraj)
	trajectories = range(lower,upper,dtraj)
	jobs.append(job_server.submit(calcRdfs,args=(f,trajectories,flags,indices,nbins,drange),modules=("numpy as np","MDAnalysis as md","MDAnalysis.analysis.distances as mddist","import histogram"),callback=sumRdfs.add))	

# Wait for all jobs to complete
job_server.wait()
job_server.destroy()
end = time.time()
print "Time = %f" % (end-start)
print "All jobs are done."
print "Finalizing calculations."
#for job in jobs:
#	sumRdfs.add(job())
rdfs = sumRdfs.value

# Normalize RDF
radii = 0.5*(edges[1:] + edges[:-1])
vol = (4./3.)*np.pi*(np.power(edges[1:],3)-np.power(edges[:-1], 3))
area = 4.*np.pi*radii**2.
bigVol = (4./3.)*np.pi*dmax**3.
# normalization to the average density n/boxvolume in the simulation
rho = npts /boxvolume
rhoA = nA / boxvolume
rhoB = nB / boxvolume
if AA_flag:	
	norm = bigVol / vol / np.sum(rdfs['AA'][1])
	rdfs['AA'][1] *= norm
if BB_flag:
	norm = bigVol / vol / np.sum(rdfs['BB'][1])
	rdfs['BB'][1] *= norm
if AB_flag:
	norm = bigVol / vol / np.sum(rdfs['AB'][1])
	rdfs['AB'][1] *= norm

# Prepare to save data
data = []

fig = pl.figure(1)
ax = fig.add_subplot(111)
if AA_flag:
	ax.plot(rdfs['AA'][0],rdfs['AA'][1],lw=2.0,label='AA')
	data.append(rdfs['AA'][0])
	data.append(rdfs['AA'][1])
if BB_flag:
	ax.plot(rdfs['BB'][0],rdfs['BB'][1],lw=2.0,label='BB')
	data.append(rdfs['BB'][1])
if AB_flag:
	ax.plot(rdfs['AB'][0],rdfs['AB'][1],lw=2.0,label='AB')
	data.append(rdfs['AB'][1])
ax.set_xlabel(r'$r$')
ax.set_ylabel(r'$g(r)$')
ax.legend(loc='best')
fig.savefig(f+'.rdf.pdf')

# Save data
data = np.array(data).T
np.savetxt(f+'.rdf.txt',data)
del fig

# Fourier transform to get the partial structure factors
sqs = {}
if AA_flag:
	xA = nA/npts
	q,sq = fftTrans(radii,(rdfs['AA'][1]-1.),3*nbins)
	sq = xA + rho*xA*xA*sq
	# Calculate form factor
	ff = formFactor(q/2./np.pi,rbig)
	sqs['AA'] = [q,sq,ff]
if BB_flag:
	xB = nB/npts
	q,sq = fftTrans(radii,(rdfs['BB'][1]-1.),3*nbins)
	sq = xB + rho*xB*xB*sq
	# Calculate form factor
	ff = formFactor(q/2./np.pi,rsmall)
	sqs['BB'] = [q,sq,ff]
if AB_flag:
	q,sq = fftTrans(radii,(rdfs['AB'][1]-1.),3*nbins)
	sq = rho*xA*xB*sq
	sqs['AB'] = [q,sq]

# Prepare calculation of intensity
I = np.zeros(len(q))

# Prepare to save data
data = []

fig = pl.figure(2)
ax = fig.add_subplot(111)
if AA_flag:
	ax.plot(sqs['AA'][0],sqs['AA'][1],lw=2.0,label='AA')
	data.append(sqs['AA'][0])
	data.append(sqs['AA'][1])
	I += sqs['AA'][2]**2.*sqs['AA'][1]
if BB_flag:
	ax.plot(sqs['BB'][0],sqs['BB'][1],lw=2.0,label='BB')
	data.append(sqs['BB'][1])
	I += sqs['BB'][2]**2.*sqs['BB'][1]
if AB_flag:
	ax.plot(sqs['AB'][0],sqs['AB'][1],lw=2.0,label='AB')
	data.append(sqs['AB'][1])
	I += 2.*sqs['AA'][2]*sqs['BB'][2]*sqs['AB'][1]
#ax.set_xlim(0.0,1.0)
ax.set_xlim(left=0.0)
#ax.set_ylim(bottom=-15.0)
ax.set_xlabel(r'$q$')
ax.set_ylabel(r'$S(q)$')
ax.legend(loc='best')
fig.savefig(f+'.sq.pdf')

# Save data
data = np.array(data).T
np.savetxt(f+'.sq.txt',data)
del fig

# Prepare plotting the intensity
fig = pl.figure(3)
ax = fig.add_subplot(111)
ax.plot(q,I,lw=2.0)
ax.set_xlim(left=0.0)
ax.set_xlabel(r'$q$')
ax.set_ylabel(r'$I(q)$')
fig.savefig(f+'.iq.pdf')
# Save data
np.savetxt(f+'.Iq.txt',np.array([q,I]).T)

#pl.show()
#raw_input('Press <ENTER> to continue') 