#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://www.scorec.rpi.edu/wiki/SLURM
# http://www.open-mpi.org/faq/?category=slurm
# http://wiki.ccni.rpi.edu/index.php/SLURM
# http://manpages.ubuntu.com/manpages/hardy/man5/slurm.conf.5.html
# http://manpages.ubuntu.com/manpages/jaunty/man1/sbatch.1.html
# https://computing.llnl.gov/linux/slurm/cons_res.html
# https://computing.llnl.gov/linux/slurm/faq.html

# http://172.20.64.30/slurm/configurator.html

import os, datetime, shlex, subprocess, sys
from optparse import OptionParser

class submitter:
    def __init__(self):
        # Intialize parser
        self.parser = OptionParser()
        self.parser.add_option("-s", "--submit", action="store_true", dest="submit",     default=False,          help="Submit job, don't just print the script. [default: %default]")
        self.parser.add_option(      "--np",     type=int,            dest="np",         default=None,           help="Number of CPUs to use (16 per node available) [default: %default]")
        self.parser.add_option(      "--nodes",  type=int,            dest="nb_nodes",   default=None,           help="Number of nodes to reserve for job (optional) [default: %default]")
        self.parser.add_option(      "--pernode",type=int,            dest="cpus_per_node", default=None,        help="Number of cpu per node (optional) [default: %default]")
        self.parser.add_option("-x", "--exclusive", action="store_true", dest="exclusive", default=False,        help="Reserve whole node(s). Will prevent other jobs from running on the allocated node(s). [default: %default]")
        self.parser.add_option("-p", "--program",type=str,            dest="program",    default="./helloworld", help="Program to run [default: %default]")
        self.parser.add_option("-a", "--args",   type=str,            dest="arguments",  default="",             help="Program's arguments. Quote them!!! [default: %default]")
        self.parser.add_option("-n", "--name",   type=str,            dest="name",       default="SimName",      help="Job's name [default: %default]")
        self.parser.add_option("-t", "--type",   type=str,            dest="type",       default="serial",       help="Job's running type (serial, mpi, openmp) [default: %default]")
        self.parser.add_option("-q", "--queue",  type=str,            dest="partition",  default="normalnodes",  help="Partition to use (wholecluster, normalnodes, supernodes) [default: %default]")
        self.parser.add_option(      "--partition",type=str,          dest="partition",  default="normalnodes",  help="Partition to use (wholecluster, normalnodes, supernodes) [default: %default]")
        self.parser.add_option("-g", "--gpu",    type=int,            dest="gpu",        default=None,           help="Request a GPU [default: %default]")
        self.parser.add_option("-e", "--email",  type=str,            dest="email",      default=None,           help="Email to send notifications [default: %default]")
        self.parser.add_option("-k", "--kind",   action="append", type=str, dest="kind", default=None,           help="Email notifications type (see sbatch's man file under '--mail-type') [default: [\"END\", \"FAIL\"]]")
        self.parser.add_option("-o", "--output", type=str,            dest="output_folder", default="output",    help="Output folder where submission script, job's standard output and standard error are saved. [default: %default]")
        
        self.submission_script_content = None
        self.options = None

    def setParameters(self,submit_string):
        (options, args) = self.parser.parse_args(shlex.split(submit_string))
        self.options = options

        # Make sure output folder exists
        if (not os.path.exists(options.output_folder)):
            os.makedirs(options.output_folder)

        max_np_per_node = 16

        # ************************************************************
        # ******** Parameters ****************************************

        # Save all output to these files. %j is replaced by the job #
        log_error  = options.output_folder + "/err_%j.log"
        log_output = options.output_folder + "/out_%j.log"

        # ***************************************************************************************************************
        # Error checking


        if (options.type != "serial" and options.type != "mpi" and options.type != "omp"):
            parser.error("Error: Valid options for --type are: serial, mpi, omp")

        if (options.type == "serial"):
            if (options.np != None or options.nb_nodes != None):
                print "############################################################"
                print "#    WARNING: Serial jobs will execute on a single CPU.    #"
                print "#    For a parallel job, specify --type=[mpi/openmp]       #"
                print "#    For a serial job, DON'T specify --np or --nodes       #"
                print "############################################################"
                options.np = None
                options.nb_nodes = None
        else:
            if (options.type == "mpi"):
                if (options.np == None and options.nb_nodes == None):
                    parser.error("You need to specify at least one of these options:\n"
                                + "    --np=[number of processors]\n    --nodes=[number of nodes]")
            elif (options.type == "omp"):
                if (options.np == None):
                    parser.error("You need to specify the number of threads:\n"
                                + "    --np=[number of processors]")
            if (options.type == "omp" and (options.np != None and options.np > 16) ):
                parser.error("Error: OpenMP has a max of 16 threads.\nChange option '--np'")
            if (options.nb_nodes != None and options.nb_nodes <= 0):
                parser.error("Error: Number of nodes must be >= 1.\nChange option '--nodes'")
            if (options.np != None and options.np <= 0):
                parser.error("Error: Number of CPU must be >= 1.\nChange option '--np'")
            if (options.np != None and options.nb_nodes != None):
                if (options.nb_nodes > options.np):
                    parser.error("Error: Number of processor must be at least the same as number of nodes!")
                if (options.np/float(options.nb_nodes) > max_np_per_node):
                    parser.error("Error: Number of processor per node must be less then " + str(max_np_per_node) + "!")

        # ***************************************************************************************************************
        # OpenMPI is compiled with slurm support. No need to specify any option to mpirun.
        if (options.type == "mpi"):
            to_run = "mpirun "
        else:
            to_run = ""
        to_run += options.program + " " + options.arguments

        # ***************************************************************************************************************
        if (options.type == "omp"):
            openmp_string = "#SBATCH --sockets-per-node=" + str(options.np) + "\n"  # Force all threads to run on the same node
            openmp_string = "#SBATCH --nodes=1-1\n"
            openmp_string += "export OMP_NUM_THREADS=" + str(options.np) + "\n\n"
        else:
            openmp_string = "\n"

        # ***************************************************************************************************************
        if (options.np != None):
            np_string = "#SBATCH --ntasks=" + str(options.np) + "\n"
        else:
            np_string = ""

        # ***************************************************************************************************************
        if (options.nb_nodes != None):
            nb_nodes_string = "#SBATCH --nodes=" + str(options.nb_nodes) + "-" + str(options.nb_nodes) + "\n"
        else:
            nb_nodes_string = ""

        # ***************************************************************************************************************
        if (options.cpus_per_node != None):
            cpus_per_node_string = "#SBATCH --ntasks-per-node=" + str(options.cpus_per_node) + "\n"
        else:
            cpus_per_node_string = ""

        # ***************************************************************************************************************
        if (options.exclusive):
            exclusive_string = "#SBATCH --exclusive\n"
        else:
            exclusive_string = ""

        # ***************************************************************************************************************
        if (options.gpu != None):
            gpu_string = "#SBATCH --gres=gpu:" + str(options.gpu) + "\n"
        else:
            gpu_string = ""

        # ***************************************************************************************************************
        # Email notifications.
        if (options.email != None):
            if (options.kind != None):
                email_types = options.kind
            else:
                email_types = ["END", "FAIL"]
            email_address = options.email
            email_string = "#SBATCH"
            for email_type in email_types:
                email_string += " --mail-type=" + email_type
            email_string += "\n#SBATCH --mail-user=" + email_address + "\n"
        else:
            email_string = ""


        # ************************************************************
        # ******** Submit job ****************************************
        # The following just assamble a string with the right parameters,
        # saves it to a file and call "sbatch" on it to submit the job.
        # ***************************************************************************************************************
        #    + "--exclude=nodeGPU[7,11-14,15,16]"""  + "\n" \

        self.submission_script_content =   "#!/bin/bash\n\n" \
        			    + "#SBATCH --job-name=""" + options.name + "\n" \
                        + "#SBATCH --error=""" + log_error + "\n" \
                        + "#SBATCH --output=""" + log_output + "\n" \
                        + "#SBATCH --partition=""" + options.partition + "\n" \
                        + np_string \
                        + nb_nodes_string \
                        + cpus_per_node_string \
                        + exclusive_string \
                        + gpu_string \
                        + email_string \
                        + openmp_string \
                        + to_run \
                        + "\n"
        return self.submission_script_content

    def submit(self):
        now = datetime.datetime.now().strftime("%Y%m%d_%Hh%M")
        submission_file = self.options.output_folder + "/slurm_" + now + ".sh"
        f = open(submission_file, 'w')
        f.write(self.submission_script_content)
        f.close()

        command = "sbatch " + submission_file
        pid = subprocess.Popen(shlex.split(command), stdout=sys.stdout, stderr=sys.stdout)
        stdout, stderr = pid.communicate()
