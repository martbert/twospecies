#############################################################
# Import libraries
#############################################################

from hoomd_script import *
from hoomd_plugins import custom_pair_potentials as custpair
from optparse import OptionParser, OptionGroup
import math,random
import sys,os,re

#import numpy as np

#############################################################
# Extending the parser with custom parsing arguments parser definition
# This only works with a custom version implemented by MB
#############################################################
try:
	custom_parser = OptionGroup(globals.parser, "Custom Options","This group contains all user defined options.")
	custom_parser.add_option("-f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("-n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("-d", type="int", dest="depl_flag", help="Custom option: depletant flag")
	custom_parser.add_option("-t", type="float", dest="temp", help="Custom option: temperature")

	globals.parser.add_option_group(custom_parser)

	# The command line parser needs to be manually called (allows greater flexibility)
	init.parse_command_line()

	if globals.options.filename:
		name = globals.options.filename
	else:
		globals.parser.error("Need a filename to execute simulation")

	if globals.options.int_steps:
		int_steps = globals.options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if globals.options.depl_flag:
		depl_flag = globals.options.depl_flag
	else:
		depl_flag = 1

	if globals.options.temp:
		kT = globals.options.temp
	else:
		kT = 1.0

except:
	custom_parser = OptionParser()
	custom_parser.add_option("--f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("--n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("--d", type="int", dest="depl_flag", help="Custom option: depletant flag")
	custom_parser.add_option("--t", type="float", dest="temp", help="Custom option: temperature")

	custom_args = option.get_user()
	(options, args) = custom_parser.parse_args(custom_args)

	if options.filename:
		name = options.filename
	else:
		parser.error("Need a filename to execute simulation")

	if options.int_steps:
		int_steps = options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if options.depl_flag:
		depl_flag = options.depl_flag
	else:
		depl_flag = 1

	if options.temp:
		kT = options.temp
	else:
		kT = 1.0

#############################################################
# Routines
#############################################################

# Tabulated LJ potential with force cap
def tablj(r, rmin, rmax, epsilon, sigma, delta, cap):
     V = 4 * epsilon * ( (sigma / (r-delta))**12 - (sigma / (r-delta))**6);
     F = 4 * epsilon / (r-delta) * ( 12 * (sigma / (r-delta))**12 - 6 * (sigma / (r-delta))**6);
     F = min(F,cap)
     return (V, F)
	
#############################################################
# Main
#############################################################

# Some flags for the script
#############################################################
#Simulation flags
restart_flag = 0
overlap_flag = 1
slj_flag = 1
self_diff_flag = 0

# Set some values
#############################################################
overlap_steps = 1000
overlap_runs = 24
warmup_steps = 10000
warmup_times = 1

pi = 3.14159265

# Parameters of the simulation
#############################################################=

#rc = 2.**(1./6.)
# Attractive part also
rc = 3.5
dt = 0.005

dbig = 32.0
dsmall = 16.0

#kT = [1.0,0.95,0.9,0.85,0.8,0.75,0.7,0.65,0.6,0.55,0.5,0.45]
#kT = [0.7,0.68,0.66,0.64,0.62]
kT = [0.8,0.77,0.74,0.71,0.68,0.65,0.62,0.59,0.56,0.53,0.5]
#kT = [1.0,0.4]

# Initialize system
#############################################################=
# Data output path
#data_path = '/scratch/mbertram/'
#data_path = '/home/martbert/Documents/depletion/data/'
#data_path = '/Users/martinbertrand/Documents/Recherche/twoSpecies/data/'
data_path = '/raid/martin/data/twoSpecies/'
dir_path = data_path+name
# Verify if dir path exists, if not create
while True:
	try:
		if not os.path.exists(dir_path):
			os.mkdir(dir_path)
	except:
		print "Trying to create folder"
	else:
		print "Folder created"
		break

# A new system is being setup
system = init.read_xml(filename="xmlfiles/%s.xml" % (name))

# Box dimension
box = system.box

# Find the maximum diameter
dmax = 0.0
for p in system.particles:
	d = p.diameter
	if d > dmax:
		dmax = d

# Setup interactions
#############################################################

# Non-bonded interactions
#####################
# The particle types are:
bigtype = 'A'
smalltype = 'B'

#slj = pair.slj(r_cut=rc, d_max = dbig)
#slj.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, sigma=1.0)

if not self_diff_flag:
	syukawa = custpair.pair.syukawa(r_cut=rc, d_max = dmax)
	syukawa.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, kappa=10.0)

	sgauss = custpair.pair.sgauss(r_cut=rc, d_max = dmax)
	sgauss.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=0.0, sigma=0.5)
else:
	lj = pair.lj(r_cut=1.12246)
	lj.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=0.0, sigma=1.0)

# Declare useful groups
#############################################################
# Polymer particles
gbig = group.type(name='big',type=bigtype)
gsmall = group.type(name='small',type=smalltype)
gall = group.all()
	
# Output a mol2 snapshot
mol2 = dump.mol2()

# Reset exclusions
if 1:
	nlist.reset_exclusions(exclusions = ['diameter'])
	nlist.set_params(d_max = dmax)

# Warmup integration (only if it's not a restart)
#############################################################

standard = integrate.mode_standard(dt=dt)
zeroer = update.zero_momentum(period=1E7)

nvt = integrate.nvt(group=gall,tau=1.0,T=kT[0])
nve = integrate.nve(group=gall)
nvt.disable()

# Main integration
#############################################################

quantities = ['time',
			  'kinetic_energy',
			  'pressure_xx',
			  'pressure_yy',
			  'pressure_zz',
			  'pressure_xy',
			  'pressure_xz',
			  'pressure_yz']

for j in range(len(kT)):
	T = kT[j]
	fname = "%s/%s.T%.2f" % (dir_path,name,T)
	if self_diff_flag:
		fname += '.sd'

	# Change depth of the attractive well
	if not self_diff_flag:
		egauss = -10.*(1.-T)
		sgauss.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=egauss, sigma=0.5)

	# Ramp temperature to desired value if T > kT[0]
	if j > 0:
		nve.disable()
		nvt.enable()

		TRamp = variant.linear_interp(points = [(0, kT[j-1]), (1e5, T)])
		nvt.set_params(tau=1.0, T=TRamp)
		
		run(2e5)
		
		nvt.disable()
		nve.enable()

	# Analyzers
	while True:
		try:
			logger = analyze.log(filename=fname+'.log', period=100,quantities=quantities, header_prefix='#')
		except:
			print "Trying to create log file"
		else:
			print "Log file created"
			break
	while True:
		try:
			msd = analyze.msd(groups=[gbig, gsmall], period=100, filename=fname+'.msd.log', header_prefix='#')
		except:
			print "Trying to create msd file"
		else:
			print "Msd file created"
			break

	# Dump
	while True:
		try:
			mol2.write(filename=fname+'_main.mol2')
		except:
			print "Trying to create mol2 file"
		else:
			print "Mol2 file created"
			break
	while True:
		try:
			dcd = dump.dcd(filename=fname+'_main.dcd', group=gall, period=500, unwrap_full=True)
		except:
			print "Trying to create dcd file"
		else:
			print "DCD file created"
			break

	# Run
	for i in range(int_times):	
		run(int_steps)

	# Dump restart xml
	while True:
		try:
			dump.xml(filename=fname+'.restart.xml', all=True)
		except:
			print "Trying to write XML file"
		else:
			print "XML file written"
			break

	# Disable and delete analyzers
	logger.disable()
	msd.disable()
	dcd.disable()
	del logger
	del msd
	del dcd