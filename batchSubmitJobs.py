#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, getopt, os, shlex, itertools
from optparse import OptionParser
#import numpy as np

import submissionModule as subm

###############################################################
# Get command line arguments
###############################################################
parser = OptionParser()
parser.add_option("-f", "--file", dest="basenames",help="input base file", metavar="FILE")
parser.add_option("-w", default=False,action="store_true", dest="warmup")
parser.add_option("-r", default=False,action="store_true", dest="run")

(options, args) = parser.parse_args()


############################################################
# Routines
############################################################

############################################################
# Main body
############################################################

# Initialize submitter
############################################################
s = subm.submitter()

# Paths to check for file existence
############################################################
data_path = '/raid/martin/data/twoSpecies/'

# Constants
############################################################
kTWarmup = '1.0'

# Variables
############################################################
# Combinations
#comb = list(itertools.product(*[basenames,rt]))

# Loop through combinations and create submit list
############################################################

if options.basenames:
	basenames = options.basenames.split(',')
else:
	# Manual entry
	basenames = ['bigSmallBoth.b500.s4000.p0.03']

for basename in basenames:
	if options.warmup:
		submit_string = """-p hoomd -a 'twoSpecies.warm.hoomd.py --user="--f %s --n 10000000 --t 1.0" --gpu=0' -q gpu -g 1 -o twoSpecies -n twoSpecies -s""" % (basename)
		script = s.setParameters(submit_string)
		# Submit the simulation
		s.submit()

	elif options.run:
		dir_path = data_path + basename+'.go'
		if (not os.path.exists(dir_path)):
			submit_string = """-p hoomd -a 'twoSpecies.RampT2.hoomd.py --user="--f %s.go --n 20000000" --gpu=0' -q gpu -g 1 -o twoSpecies -n twoSpecies -s""" % (basename)
			script = s.setParameters(submit_string)
			# Submit the simulation
			s.submit()

	else:
		print "Problem!"