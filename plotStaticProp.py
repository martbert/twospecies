#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
import histogram
from scipy import optimize

import numpy as np
import pylab as pl
import matplotlib_params
import matplotlib.cm as cm

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

self_flag = 0

for o, val in opts:
	if o == "-f":
		pfx = val

#pfx = 'results/bigSmallO.b500.s4000.go.T0.65_main'

############################################################
# Main body
############################################################

gr = np.loadtxt(pfx+'.rdf.txt')
sq = np.loadtxt(pfx+'.sq.txt')
iq = np.loadtxt(pfx+'.iq.txt')

# Plot rdf	
fig = pl.figure(1)
ax = fig.add_subplot(111)

ax.plot(gr[:,0],gr[:,1],'-',lw=2.0,label='S-S')
ax.plot(gr[:,0],gr[:,2],'-',lw=2.0,label='B-B')
ax.plot(gr[:,0],gr[:,3],'-',lw=2.0,label='S-B')
ax.set_xlabel(r'$r$')
ax.set_ylabel(r'$g(r)$')
ax.set_xlim(15,60)
leg = ax.legend(loc='best',fancybox=True)
leg.get_frame().set_alpha(0.65)
fig.savefig(pfx+'.rdf.pdf')

# Plot sq
fig = pl.figure(2)
ax = fig.add_subplot(111)

ax.plot(sq[:,0],sq[:,1],'-',lw=2.0,label='S-S')
ax.plot(sq[:,0],sq[:,2],'-',lw=2.0,label='B-B')
ax.plot(sq[:,0],sq[:,3],'-',lw=2.0,label='S-B')
ax.set_xlabel(r'$q$')
ax.set_ylabel(r'$S(q)$')
ax.set_xlim(0,6)
ax.set_ylim(-1,3.2)
leg = ax.legend(loc='best',fancybox=True)
leg.get_frame().set_alpha(0.65)
fig.savefig(pfx+'.sq.pdf')

pl.show()