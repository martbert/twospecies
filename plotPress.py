#!/usr/bin/env python

import sys, getopt, os
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize
import re

import matplotlib_params

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
	opts, args = getopt.getopt(argv,"f:")
except getopt.GetoptError:
	usage()
	sys.exit(2)

for o, val in opts:
	if o == "-f":
		fileList = val

############################################################
# Routines and functions
############################################################

flin = lambda x,a,b: a*x+b

############################################################
# Main body
############################################################

# Percent to skip (brute force initially)
skipfrac = 0.2

# Load list of filenames
fnames = np.loadtxt(fileList,dtype=str)
nfiles = len(fnames)

# Prepare to plot all
fig1 = pl.figure(1)
ax1 = fig1.add_subplot(111)

# Loop over the files, load the data and find the diffusion coefficient
Dbig = np.zeros(nfiles)
Dsmall = np.zeros(nfiles)
kT = np.zeros(nfiles)
for i in range(nfiles):
	fname = fnames[i]
	# Get temperature
	name = fname[fname.rfind('/')+1:]
	sysvars = np.array(re.findall(r"[-+]?\d*\.\d+|\d+", name)).astype(float)
	kT[i] = sysvars[-1]
	# Load data
	data = np.loadtxt(fname)
	npts = data.shape[0]
	ax1.plot(data[:,0],data[:,1])
	# Calculate amount of points to skip
	skip = int(skipfrac*npts)
	# Prepare data to fit
	time = data[skip:,0]
	msdbig = data[skip:,1]
	msdsmall = data[skip:,2]
	# Fit the mean square displacement
	parbig,covbig = optimize.curve_fit(flin,time,msdbig,p0=[1.,1.])
	parsmall,covsmall = optimize.curve_fit(flin,time,msdsmall,p0=[1.,1.])
	Dbig[i] = parbig[0]/6.
	Dsmall[i] = parsmall[0]/6.

# Plot the results
fig2 = pl.figure(2)
ax2 = fig2.add_subplot(111)
ax2.plot(kT,Dbig,'--o',label=r'Big')
ax2.plot(kT,Dsmall,'--s',label=r'Small')
ax2.set_xlabel(r'$k_BT$')
ax2.set_ylabel(r'$D$')

