#############################################################
# Import libraries
#############################################################

from hoomd_script import *
from optparse import OptionParser, OptionGroup
from hoomd_plugins import custom_pair_potentials as custpair
import math,random
import sys,os,re

#import numpy as np
#import MDAnalysis.analysis.distances as mddist 

#############################################################
# Extending the parser with custom parsing arguments parser definition
# This only works with a custom version implemented by MB
#############################################################
try:
	custom_parser = OptionGroup(globals.parser, "Custom Options","This group contains all user defined options.")
	custom_parser.add_option("-f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("-n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("-d", type="int", dest="depl_flag", help="Custom option: depletant flag")
	custom_parser.add_option("-t", type="float", dest="temp", help="Custom option: temperature")

	globals.parser.add_option_group(custom_parser)

	# The command line parser needs to be manually called (allows greater flexibility)
	init.parse_command_line()

	if globals.options.filename:
		name = globals.options.filename
	else:
		globals.parser.error("Need a filename to execute simulation")

	if globals.options.int_steps:
		int_steps = globals.options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if globals.options.depl_flag:
		depl_flag = globals.options.depl_flag
	else:
		depl_flag = 1

	if globals.options.temp:
		kT = globals.options.temp
	else:
		kT = 1.0

except:
	custom_parser = OptionParser()
	custom_parser.add_option("--f", dest="filename", help="Custom option: prefix for files to be saved")
	custom_parser.add_option("--n", type="int", dest="int_steps", help="Custom option: number of steps")
	custom_parser.add_option("--d", type="int", dest="depl_flag", help="Custom option: depletant flag")
	custom_parser.add_option("--t", type="float", dest="temp", help="Custom option: temperature")

	custom_args = option.get_user()
	(options, args) = custom_parser.parse_args(custom_args)

	if options.filename:
		name = options.filename
	else:
		parser.error("Need a filename to execute simulation")

	if options.int_steps:
		int_steps = options.int_steps
	else:
		int_steps = 500000
	int_times = 1

	if options.depl_flag:
		depl_flag = options.depl_flag
	else:
		depl_flag = 1

	if options.temp:
		kT = options.temp
	else:
		kT = 1.0

#############################################################
# Routines
#############################################################

# Tabulated LJ potential with force cap
def tablj(r, rmin, rmax, epsilon, sigma, delta, cap):
     V = 4 * epsilon * ( (sigma / (r-delta))**12 - (sigma / (r-delta))**6);
     F = 4 * epsilon / (r-delta) * ( 12 * (sigma / (r-delta))**12 - 6 * (sigma / (r-delta))**6);
     F = min(F,cap)
     return (V, F)

"""
def calcMinDist(group,box):
	npts = len(group)
	r = []
	for p in group:
		pos = p.position
		r.append(np.array(pos))
	r = np.array(r).astype(np.float32)
	dist = mddist.self_distance_array(r,box)
	mindist = np.min(dist)
	return mindist
"""
	
#############################################################
# Main
#############################################################

# Some flags for the script
#############################################################
#Simulation flags
restart_flag = 0
overlap_flag = 1
slj_flag = 1

# Set some values
#############################################################
overlap_steps = 1000
overlap_runs = 24
warmup_steps = 10000
warmup_times = 1

pi = 3.14159265

# Parameters of the simulation
#############################################################=

rc = 3.5
dt = 0.005

# Diameters
dbig = 32.0
dsmall = 16.0

# Volumes
vbig = 4.*pi/3.*(0.5*dbig)**3.
vsmall = 4.*pi/3.*(0.5*dsmall)**3.
vunit = 4.*pi/3.*(0.5)**3.

# Baseline number of big particles
nbig = 1000

# Total volume fraction
ptot = 0.3
vtot = nbig*vbig

# System volume
vbox = vtot/ptot
newboxl = vbox**(1./3.)

# Initialize system
#############################################################=
# A new system is being setup
system = init.read_xml(filename="xmlfiles/%s.xml" % (name))

# Box dimension
box = system.box

# Find the maximum diameter
dmax = 0.0
for p in system.particles:
	d = p.diameter
	if d > dmax:
		dmax = d

# Setup interactions
#############################################################

# Non-bonded interactions
#####################
# The particle types are:
bigtype = 'A'
smalltype = 'B'

#slj = pair.slj(r_cut=rc, d_max = dbig)
#slj.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, sigma=1.0)
syukawa = custpair.pair.syukawa(r_cut=rc, d_max = dmax)
syukawa.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=1.0, kappa=10.0)

#sgauss = custpair.pair.sgauss(r_cut=rc, d_max = dbig+1.)
#sgauss.pair_coeff.set([bigtype,smalltype],[bigtype,smalltype], epsilon=0.0, sigma=0.5)

# Declare useful groups
#############################################################
# Polymer particles
gbig = group.type(name='big',type=bigtype)
gsmall = group.type(name='small',type=smalltype)
gall = group.all()
	
# Reset exclusions
nlist.reset_exclusions(exclusions = ['diameter'])
nlist.set_params(d_max = dmax)

# Warmup integration (only if it's not a restart)
#############################################################

standard = integrate.mode_standard(dt=dt)
zeroer = update.zero_momentum(period=1E7)

# Calculate minimum distance
#print "Min dist = %f" % (calcMinDist(gall,np.array(box).astype(np.float32)))

# Give velocity to all particles
for p in gbig:
	stdv = math.sqrt(kT/p.mass)
	p.velocity = (random.gauss(0.0,stdv),random.gauss(0.0,stdv),random.gauss(0.0,stdv))

for p in gsmall:
	stdv = math.sqrt(kT/p.mass)
	p.velocity = (random.gauss(0.0,stdv),random.gauss(0.0,stdv),random.gauss(0.0,stdv))

#nvt = integrate.bdnvt(group=all,T=kT,gamma_diam=True)
nvt = integrate.bdnvt(group=gall,T=kT)
nvt.set_gamma(bigtype,gamma=dbig/dbig)
nvt.set_gamma(smalltype,gamma=dsmall/dbig)
run(1E5)

# Then ramp down the volume
rampt = 1E6
L = variant.linear_interp(points = [(0, box[0]), (rampt, newboxl)], zero='now')
box_resize = update.box_resize(Lx = L, period=1)
run(rampt)
box_resize.disable()

# Calculate new volume and print
box = system.box
vol = box[0]*box[1]*box[2]
nbig = len(gbig)
nsmall = len(gsmall)
vtemp = nsmall*vsmall+nbig*vbig
print "Volume fraction = %f" % (vtemp/vol)

# Run another warmup
dt = 0.01
standard.set_params(dt=dt)
run(2E6)

# Save final xml file
while True:
	try:
		dump.xml(filename="xmlfiles/%s.go.xml" % (name),position=True,velocity=True,image=True,mass=True,diameter=True,type=True)
		run(1)
	except:
		print "Trying to write xml"
	else:
		print "xml file written"
		break	
