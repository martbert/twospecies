#!/usr/bin/env python

from __future__ import division

import sys, getopt, os, thread, pickle, time
import numpy as np
from scipy import optimize

import pylab as pl
import seaborn as sb
sb.set(font_scale=1.4, style='ticks')

import h5py
import MDAnalysis as md
import MDAnalysis.analysis.distances as mddist

import mcubes as mc

###############################################################
# Get command line arguments
###############################################################
argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv,"f:p:s")
except getopt.GetoptError:
    usage()
    sys.exit(2)

self_flag = 0

for o, val in opts:
    if o == "-f":
        f = val
        fbase = os.path.basename(f)
    if o == "-s":
        self_flag = 1
    if o == "-p":
        # Parallel environment is setup
        ncpus = int(val)
        # Import Parallel Python library
        import pp
        # Initialize job server
        ppservers = ()
        job_server = pp.Server(ncpus,ppservers=ppservers)
        print "Starting pp with", job_server.get_ncpus(), "workers"

############################################################
# Routines
############################################################

############################################################
# Main
############################################################

# Load molecular dynamics trajectory
system = md.Universe(f+'.mol2', f+'.dcd')

# Box
box = system.dimensions[:3]

# Create the two groups
gA = system.select_atoms('resname A')
gB = system.select_atoms('resname B')

# radii
rbig = 16.
rsmall = 8.
cutoff = rbig + 1.2 * rsmall

# Open the Q value data
# qdat = h5py.File(f+'_Q.h5', 'r')
qdat = h5py.File(f+'_CN.h5', 'r')

# Prepare plotting
# from mayavi import mlab
# mlab.view(azimuth=270,elevation=90,distance=200.0,focalpoint=(0,0,0))
# mlab.roll(180)

# Loop and produce a 3D picture of what is happening
ntraj = len(system.trajectory)
skip = 1000

# for frame in range(0, ntraj, skip):
if 1:
    frame = 10000
    traj = system.trajectory[frame]

    # Try it for the first frame
    Q = qdat[str(frame)][...]

    # Get coords
    coordA = np.remainder(gA.coordinates(), box)
    coordB = np.remainder(gB.coordinates(), box)

    if 0:
        # Weighted histogram of the Q values
        nbins = 20
        ranges = ((0, box[0]), (0, box[1]), (0, box[2]))

        vals, edges = np.histogramdd(coordB, bins=nbins, range=ranges, weights=Q)

        x = 0.5 * (edges[0][:-1] + edges[0][1:])
        y = 0.5 * (edges[1][:-1] + edges[1][1:])
        z = 0.5 * (edges[2][:-1] + edges[2][1:])
        X,Y,Z = np.meshgrid(x,y,z)

        # Triangulate
        vertices, triangles = mc.marching_cubes(vals, 0.3)

        x, y, z = vertices.T
        mlab.triangular_mesh(x, y, z, triangles, opacity=1.0)

    ct_mtx_AB = np.zeros((len(gA), len(gB)))
    nI = (np.eye(len(gB), dtype=bool) != True)

    mddist.distance_array(coordA, coordB, box, result=ct_mtx_AB)

    ct_mtx_AB = (ct_mtx_AB < cutoff)

    # Get indices where contact is True
    i,j = np.where(ct_mtx_AB)
    j_prox = np.unique(j)

    # Complementary where there is no contact
    j_far = np.setdiff1d(np.arange(len(coordB), dtype=int), j_prox)

    print Q[j_far].mean()
    print Q[j_prox].mean()

    pl.hist(Q[j_far][Q[j_far] !=0], bins=20)
    pl.hist(Q[j_prox][Q[j_prox] !=0], bins=20)
    
    pl.show()
    sys.exit()

    # All above 0.3
    mask = Q > 0.5

    # Those above 0.3 around the big ones
    mask_prox = 0 * mask
    mask_prox[j] = Q[j] > 0.6

    # Values for coloring purposes
    vals = mask + mask_prox

    # Sub prox from all
    # mask -= mask_prox

    # mlab.show()

    # vals = vals.flatten()
    # mask = vals > 0.3

    # X = X.flatten()
    # Y = Y.flatten()
    # Z = Z.flatten()

    # Plot
    # fig = mlab.figure()

    # pts = mlab.points3d(coordA[:,0], coordA[:,1], coordA[:,2], 
    #     color=(0.75,0.75,0.75), scale_factor=32.0, opacity=0.4)

    # pts = mlab.points3d(coordB[mask,0], coordB[mask,1], coordB[mask,2], 
    #     vals[mask], scale_factor=16.0, scale_mode='none', opacity=0.4)    

    # # cubes = mlab.points3d(X[mask], Y[mask], Z[mask], vals[mask], 
    # #     mode='cube', opacity=0.5, scale_factor=10.0, scale_mode='none',
    # #     vmin=0.3, vmax=0.57)

    # mlab.savefig(fbase+'.Qdist.'+str(frame)+'.png')

    # mlab.show()
    # mlab.close()
