#!/usr/bin/env python

import sys, getopt, os
from optparse import OptionParsers
import numpy as np
import matplotlib.pyplot as pl
from scipy import optimize
import MDAnalysis.analysis.distances as mddist 

import hoomd_tools as ht

###############################################################
# Get command line arguments
###############################################################

parser = OptionParser()
parser.add_option("-f", "--filename_prefix", dest="filename_prefix",help="filename prefix", metavar="FILENAME_PREFIX")
parser.add_option("-p", "--prop_small", dest="prop_small",help="proportion of small particles", metavar="PROP_SMALL")
parser.add_option("-d", "--polydisp", dest="polydisp",help="polydispersity", metavar="POLY_DISP")

(options, args) = parser.parse_args()

if options.filename_prefix != None:
	pfx = filename_prefix
else:
	sys.exit(2)

if options.prop_small != None:
	psmall = options.prop_small
else:
	psmall = 0.0

if options.polydisp != None:
	polydisp = options.polydisp
else:
	polydisp = 0.0

############################################################
# Routines
############################################################

def setupBigParticles(box,l,b,q,type):
	boxh = box/2.
	types = np.where(np.zeros(l,dtype=str) == '',type,'')
	charges = np.zeros(l)+q
	r = np.zeros((l,3)).astype(np.float32)
	# Setup big particles randomly without them overlapping
	for i in range(1,l):
		success = 0
		#print i
		while not success:
			r_local = np.vstack((np.random.random(3)*box-boxh,np.zeros(3))).astype(np.float32)
			dist = mddist.distance_array(r_local,r,box.astype(np.float32))
			cut = np.sum(dist[0] < b)
			if cut == 0:
				success = 1
		r[i] = r_local[0]
	return r,charges,types

def setupSmallParticles(box,n,type,ddiam,ppos=np.zeros(3),pdiam=0.0):
	boxh = box/2.
	types = np.where(np.zeros(n,dtype=str) == '',type,'')
	r = np.zeros((n,3))
	# Calculate grid values
	ns = (box/ddiam).astype(int)
	print "Num sites = %f, Num particles = %f" % (np.prod(ns),n)
	# Loop over all dimensions and add particle until n is met
	counter = 0
	for i in range(ns[0]):
		for j in range(ns[1]):
			for k in range(ns[2]):
				r_local = np.array([i*ddiam,j*ddiam,k*ddiam])-boxh
				r_local = np.vstack((r_local,np.zeros(3))).astype(np.float32)
				if pdiam > 0.0:
					dist = mddist.distance_array(r_local,ppos,box.astype(np.float32))
					cond = np.min(dist[0]) > 0.5*(ddiam+pdiam)
				else:
					cond = True
				if cond:
					r[counter] = r_local[0]
					counter += 1
					if counter >= n:
						return r,types
	return r,types

############################################################
# Main body
############################################################

# Create system (box size, length of polymer, volume fraction of small depletants)
#################################################################################

# Constants
rcut = 1.12246

# Diameters
dbig = 32.0
dsmall = 16.0

# Volumes
vbig = 4.*np.pi/3.*(0.5*dbig)**3.
vsmall = 4.*np.pi/3.*(0.5*dsmall)**3.
vunit = 4.*np.pi/3.*(0.5)**3.
mbig = vbig/vsmall
msmall = 1.0

# Baseline number of big particles
nbig = 1000

# Total volume fraction
ptot = 0.1
vtot = nbig*vbig

# System volume
vbox = vtot/ptot
boxl = vbox**(1./3.)
box = np.array([boxl,boxl,boxl])
boxh = 0.5*box

# Given the fraction of the occupied volume attributed to small particles is psmall
# calculate the number of particles for both species
vtot_small = psmall*vtot
nsmall = int(vtot_small/vsmall)
vtot_big = vtot-vtot_small
nbig = int(vtot_big/vbig)
vtemp = nsmall*vsmall+nbig*vbig
	
print "There are %d small particles and %d big ones" % (nsmall,nbig)
print "The volume they occupy is %f, a fraction %f of the total" % (vtemp,vtemp/vbox)

# Add bigger particles randomly
if nbig > 0:
	dmaxb = dbig*(1.0+polydisp)
	dminb = dbig*(1.0-polydisp)
	ds = np.random.random(nbig)*(dmaxb-dminb)+dminb
	vs = 4.*np.pi/3.*(0.5*ds)**3.
	positions,charges,types = setupBigParticles(box,nbig,dmaxb+0.5,1.0,'A')
	#positions,types = setupSmallParticles(box,nbig,'B',dbig+0.5,np.array([[0.,0.,0.]]),0.0)
	diameters = np.zeros(nbig)+ds
	masses = np.zeros(nbig)+vs/vsmall
	# Check distances
	dist = mddist.self_distance_array((positions).astype(np.float32),box.astype(np.float32))
	mindist = np.min(dist)
	cut = np.sum(dist < dbig)
	if cut == 0:
		print "\tNo two big particles are overlapping. Min dist = %f" % (mindist)
	else:
		print "\tSome big particles might overlap: %d" % (cut)
	if nsmall > 0:
		dmax = dsmall*(1.0+polydisp)
		dmin = dsmall*(1.0-polydisp)
		ds = np.random.random(nsmall)*(dmax-dmin)+dmin
		vs = 4.*np.pi/3.*(0.5*ds)**3.
		rs,ts = setupSmallParticles(box,nsmall,'B',dmax+0.5,positions,dmaxb+0.5)
		positions = np.vstack((positions,rs))
		types = np.concatenate((types,ts))
		diameters = np.concatenate((diameters,ds))
		masses = np.concatenate((masses,vs/vsmall))
elif nsmall > 0:
	dmax = dsmall*(1.0+polydisp)
	dmin = dsmall*(1.0-polydisp)
	ds = np.random.random(nsmall)*(dmax-dmin)+dmin
	vs = 4.*np.pi/3.*(0.5*ds)**3.
	rs,ts = setupSmallParticles(box,nsmall,'B',dmax+0.5,np.array([[0.,0.,0.]]),0.0)
	positions = rs
	types = ts
	diameters = np.zeros(nsmall)+ds
	masses = vs/vsmall
else:
	raise "Error: no particles in the system"

ht.write_xml(fname='%s.b%d.s%d.p%.2f.xml' % (pfx,nbig,nsmall,polydisp),box=box,positions=positions,types=types,diameters=diameters,masses=masses)
